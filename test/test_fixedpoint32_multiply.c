/*
 * test_fixedpoint32_multiply.c - Unit tests: 32-bit signed fixed-point multiplication.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <math.h>
#include "test.h"
#include "fixedpoint32.h"
#include "catch.hpp"


/* PUBLIC MACROS *************************************************************/

/*! Maximum difference for equality test when testing Q16.16 fixed-point values */
#define EPSILON_Q16_16 (1.0 / 65536.0)


/* UNIT TEST DEFINITIONS *****************************************************/

SCENARIO("Fixed-point: tests for test_floatToFixedpoint32() function", "[test_floatToFixedpoint32]")
{
    char msg[120U] = {0};

    struct {double floatValue; int8_t fracLen; tFixedpoint32 expectedFp;} testVector[26U] =
    {
        /* Q16.16 fixed-point values */
        {.floatValue = 0.0,            .fracLen = 16, .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 16U}},
        {.floatValue = 1.0 / 65536.0,  .fracLen = 16, .expectedFp = {.data = {0x01U, 0x00U, 0x00U, 0x00U}, .fracLen = 16U}},
        {.floatValue = 0.5,            .fracLen = 16, .expectedFp = {.data = {0x00U, 0x80U, 0x00U, 0x00U}, .fracLen = 16U}},
        {.floatValue = 1.0,            .fracLen = 16, .expectedFp = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 16U}},
        {.floatValue = 32767.75,       .fracLen = 16, .expectedFp = {.data = {0x00U, 0xC0U, 0xFFU, 0x7FU}, .fracLen = 16U}},
        {.floatValue = -1.0 / 65536.0, .fracLen = 16, .expectedFp = {.data = {0xFFU, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 16U}},
        {.floatValue = -0.5,           .fracLen = 16, .expectedFp = {.data = {0x00U, 0x80U, 0xFFU, 0xFFU}, .fracLen = 16U}},
        {.floatValue = -0.75,          .fracLen = 16, .expectedFp = {.data = {0x00U, 0x40U, 0xFFU, 0xFFU}, .fracLen = 16U}},
        {.floatValue = -1.0,           .fracLen = 16, .expectedFp = {.data = {0x00U, 0x00U, 0xFFU, 0xFFU}, .fracLen = 16U}},
        {.floatValue = -32768.0,       .fracLen = 16, .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 16U}},
        /* Q28.4 fixed-point values */
        {.floatValue = 0.0,            .fracLen = 4,  .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 4U}},
        {.floatValue = 0.5,            .fracLen = 4,  .expectedFp = {.data = {0x08U, 0x00U, 0x00U, 0x00U}, .fracLen = 4U}},
        {.floatValue = 1.0,            .fracLen = 4,  .expectedFp = {.data = {0x10U, 0x00U, 0x00U, 0x00U}, .fracLen = 4U}},
        {.floatValue = 134217727.9375, .fracLen = 4,  .expectedFp = {.data = {0xFFU, 0xFFU, 0xFFU, 0x7FU}, .fracLen = 4U}},
        {.floatValue = -0.5,           .fracLen = 4,  .expectedFp = {.data = {0xF8U, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 4U}},
        {.floatValue = -1.0,           .fracLen = 4,  .expectedFp = {.data = {0xF0U, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 4U}},
        {.floatValue = -134217728.0,   .fracLen = 4,  .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 4U}},
        /* Q1.31 fixed-point values */
        {.floatValue = 0.0,            .fracLen = 31, .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 31U}},
        {.floatValue = 0.5,            .fracLen = 31, .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x40U}, .fracLen = 31U}},
        {.floatValue = -0.5,           .fracLen = 31, .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0xC0U}, .fracLen = 31U}},
        {.floatValue = -1.0,           .fracLen = 31, .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 31U}},
        /* Q32.0 fixed-point values */
        {.floatValue = 0.0,            .fracLen = 0,  .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 0U}},
        {.floatValue = 1.0,            .fracLen = 0,  .expectedFp = {.data = {0x01U, 0x00U, 0x00U, 0x00U}, .fracLen = 0U}},
        {.floatValue = 2147483647.0,   .fracLen = 0,  .expectedFp = {.data = {0xFFU, 0xFFU, 0xFFU, 0x7FU}, .fracLen = 0U}},
        {.floatValue = -1.0,           .fracLen = 0,  .expectedFp = {.data = {0xFFU, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 0U}},
        {.floatValue = -2147483648.0,  .fracLen = 0,  .expectedFp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 0U}}
    };

    for(size_t i = 0U; i < (sizeof(testVector) / sizeof(testVector[0U])); i++)
    {
        snprintf(msg, sizeof(msg), "Float value is %f and fixed-point fraction length is %d", testVector[i].floatValue, testVector[i].fracLen);
        WHEN(msg)
        {
            tFixedpoint32 fp = test_floatToFixedpoint32(testVector[i].floatValue, testVector[i].fracLen);
            snprintf(msg, sizeof(msg), "Fixed-point: .data = {0x%02X, 0x%02X, 0x%02X, 0x%02X}, .fracLen = %u",
                     testVector[i].expectedFp.data[0], testVector[i].expectedFp.data[1],
                     testVector[i].expectedFp.data[2], testVector[i].expectedFp.data[3],
                     testVector[i].expectedFp.fracLen);
            THEN(msg)
            {
                REQUIRE(testVector[i].expectedFp.fracLen == fp.fracLen);
                REQUIRE(test_byteArraysAreEqual(testVector[i].expectedFp.data, fp.data, sizeof(fp.data)));
            }
        }
    }
}


SCENARIO("Fixed-point: tests for test_fixedpoint32ToFloat() function", "[test_fixedpoint32ToFloat]")
{
    char msg[100] = {0};

    struct {tFixedpoint32 fp; double expectedFloatValue;} testVector[26U] =
    {
        /* Q16.16 fixed-point values */
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 16U}, .expectedFloatValue = 0.0},
        {.fp = {.data = {0x01U, 0x00U, 0x00U, 0x00U}, .fracLen = 16U}, .expectedFloatValue = 1.0 / 65536.0},
        {.fp = {.data = {0x00U, 0x80U, 0x00U, 0x00U}, .fracLen = 16U}, .expectedFloatValue = 0.5},
        {.fp = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 16U}, .expectedFloatValue = 1.0},
        {.fp = {.data = {0x00U, 0xC0U, 0xFFU, 0x7FU}, .fracLen = 16U}, .expectedFloatValue = 32767.75},
        {.fp = {.data = {0xFFU, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 16U}, .expectedFloatValue = -1.0 / 65536.0},
        {.fp = {.data = {0x00U, 0x80U, 0xFFU, 0xFFU}, .fracLen = 16U}, .expectedFloatValue = -0.5},
        {.fp = {.data = {0x00U, 0x40U, 0xFFU, 0xFFU}, .fracLen = 16U}, .expectedFloatValue = -0.75},
        {.fp = {.data = {0x00U, 0x00U, 0xFFU, 0xFFU}, .fracLen = 16U}, .expectedFloatValue = -1.0},
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 16U}, .expectedFloatValue = -32768.0},
        /* Q28.4 fixed-point values */
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 4U}, .expectedFloatValue = 0.0},
        {.fp = {.data = {0x08U, 0x00U, 0x00U, 0x00U}, .fracLen = 4U}, .expectedFloatValue = 0.5},
        {.fp = {.data = {0x10U, 0x00U, 0x00U, 0x00U}, .fracLen = 4U}, .expectedFloatValue = 1.0},
        {.fp = {.data = {0xFFU, 0xFFU, 0xFFU, 0x7FU}, .fracLen = 4U}, .expectedFloatValue = 134217727.9375},
        {.fp = {.data = {0xF8U, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 4U}, .expectedFloatValue = -0.5},
        {.fp = {.data = {0xF0U, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 4U}, .expectedFloatValue = -1.0},
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 4U}, .expectedFloatValue = -134217728.0},
        /* Q1.31 fixed-point values */
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 31U}, .expectedFloatValue = 0.0},
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x40U}, .fracLen = 31U}, .expectedFloatValue = 0.5},
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0xC0U}, .fracLen = 31U}, .expectedFloatValue = -0.5},
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 31U}, .expectedFloatValue = -1.0},
        /* Q32.0 fixed-point values */
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x00U}, .fracLen = 0U}, .expectedFloatValue = 0.0},
        {.fp = {.data = {0x01U, 0x00U, 0x00U, 0x00U}, .fracLen = 0U}, .expectedFloatValue = 1.0},
        {.fp = {.data = {0xFFU, 0xFFU, 0xFFU, 0x7FU}, .fracLen = 0U}, .expectedFloatValue = 2147483647.0},
        {.fp = {.data = {0xFFU, 0xFFU, 0xFFU, 0xFFU}, .fracLen = 0U}, .expectedFloatValue = -1.0},
        {.fp = {.data = {0x00U, 0x00U, 0x00U, 0x80U}, .fracLen = 0U}, .expectedFloatValue = -2147483648.0}
    };

    for(size_t i = 0; i < (sizeof(testVector) / sizeof(testVector[0])); i++)
    {
        snprintf(msg, 100, "Fixed-point value: .data = {0x%02X, 0x%02X, 0x%02X, 0x%02X}, .fracLen = %u",
                 testVector[i].fp.data[0U], testVector[i].fp.data[1U],
                 testVector[i].fp.data[2U], testVector[i].fp.data[3U],
                 testVector[i].fp.fracLen);
        WHEN(msg)
        {
            double floatValue = test_fixedpoint32ToFloat(testVector[i].fp);
            snprintf(msg, 100, "Float value %lu is %f", i, testVector[i].expectedFloatValue);
            THEN(msg)
            {
                REQUIRE_THAT(floatValue, Catch::Matchers::WithinAbs(testVector[i].expectedFloatValue, EPSILON));
            }
        }
    }
}


SCENARIO("Fixed-point: tests for test_byteArraysAreEqual() function", "[test_byteArraysAreEqual]")
{
    WHEN("Two byte arrays contain identical data")
    {
        uint8_t u[4] = {0, 27, 255, 84};
        uint8_t v[4] = {0, 27, 255, 84};

        THEN("test_byteArraysAreEqual() shall return true")
        {
            REQUIRE(test_byteArraysAreEqual(u, v, sizeof(u)));
        }
    }

    WHEN("The first byte is different in two byte arrays")
    {
        uint8_t u[4] = {0, 27, 255, 84};
        uint8_t v[4] = {5, 27, 255, 84};

        THEN("test_byteArraysAreEqual() shall return false")
        {
            REQUIRE_FALSE(test_byteArraysAreEqual(u, v, sizeof(u)));
        }
    }

    WHEN("The second byte is different in two byte arrays")
    {
        uint8_t u[4] = {0, 27, 255, 84};
        uint8_t v[4] = {0, 100, 255, 84};

        THEN("test_byteArraysAreEqual() shall return false")
        {
            REQUIRE_FALSE(test_byteArraysAreEqual(u, v, sizeof(u)));
        }
    }

    WHEN("The third byte is different in two byte arrays")
    {
        uint8_t u[4] = {0, 27, 255, 84};
        uint8_t v[4] = {0, 27, 7, 84};

        THEN("test_byteArraysAreEqual() shall return false")
        {
            REQUIRE_FALSE(test_byteArraysAreEqual(u, v, sizeof(u)));
        }
    }

    WHEN("The fourth byte is different in two byte arrays")
    {
        uint8_t u[4] = {0, 27, 255, 84};
        uint8_t v[4] = {0, 27, 255, 85};

        THEN("test_byteArraysAreEqual() shall return false")
        {
            REQUIRE_FALSE(test_byteArraysAreEqual(u, v, sizeof(u)));
        }
    }
}


SCENARIO("Fixed-point multiply: tests for Q16.16 input data", "[multiply_Q16.16]")
{
    char msg[120U] = {0};

    /* generate 'u' input data values */
    double uFloat[14] =
    {
        MIN_Q16_16_VALUE,
        -10000.0,
        -100.0,
        -10.5,
        -1.75,
        -0.25,
        0.0,
        0.5,
        3.25,
        10.0,
        105.0,
        2000.0,
        10001.0,
        MAX_Q16_16_VALUE
    };

    /* generate 'v' input data values */
    double vFloat[14] =
    {
        MIN_Q16_16_VALUE,
        -10010.0,
        -170.0,
        -10.25,
        -1.5,
        -0.75,
        0.0,
        0.5,
        3.5,
        10.5,
        120.25,
        2050.5,
        10007.0,
        MAX_Q16_16_VALUE
    };

    for(size_t i = 0U; i < (sizeof(uFloat) / sizeof(uFloat[0])); i++)
    {
        tFixedpoint32 u = test_floatToFixedpoint32(uFloat[i], 16U);
        snprintf(msg, sizeof(msg), "The first input value is %f in Q16.16 fixed-point format", uFloat[i]);
        WHEN(msg)
        {
            for(size_t j = 0U; j < (sizeof(vFloat) / sizeof(vFloat[0])); j++)
            {
                tFixedpoint32 v = test_floatToFixedpoint32(vFloat[j], 16U);
                snprintf(msg, sizeof(msg), "The second input value is %f in Q16.16 fixed-point format", vFloat[j]);
                AND_WHEN(msg)
                {
                    double expectedProductFloat = uFloat[i] * vFloat[j];
                    bool overflow = ((MIN_Q16_16_VALUE > expectedProductFloat) || (MAX_Q16_16_VALUE < expectedProductFloat));
                    if(overflow)
                    {
                        /* arithmetic overflow has occurred */
#if ((defined TEST) && (!defined NDEBUG))
                        snprintf(msg, sizeof(msg), "Multiplying input values %f and %f shall cause assert failure because product is out-of-range", uFloat[i], vFloat[j]);
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                        snprintf(msg, sizeof(msg), "Multiplying input values %f and %f shall cause FIXEDPOINT32_OVERFLOW", uFloat[i], vFloat[j]);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                    }
                    else
                    {
                        snprintf(msg, 100, "The product of input values %f and %f shall be %f in Q16.16 fixed point format",
                                 uFloat[i], vFloat[j], expectedProductFloat);
                    }
                    THEN(msg)
                    {
                        int8_t err = 0xFF;
                        if(overflow)
                        {
#if ((defined TEST) && (!defined NDEBUG))
            REQUIRE_THROWS_WITH( fixedpoint32_multiply(u, v, &err),
                                 Catch::Matchers::StartsWith("assert failed") );
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                            (void) fixedpoint32_multiply(u, v, &err);
                            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                        }
                        else
                        {
                            // UUT
                            tFixedpoint32 product = fixedpoint32_multiply(u, v, &err);
                            REQUIRE_THAT(test_fixedpoint32ToFloat(product), Catch::Matchers::WithinAbs(expectedProductFloat, EPSILON_Q16_16));
                            REQUIRE(FIXEDPOINT32_OK == err);
                            REQUIRE(16U == product.fracLen);
                        }
                    }
                }
            }
        }
    }
}


SCENARIO("Fixed-point multiply: tests for Q16.16 and Q8.24 input data", "[multiply_Q16.16_and_Q8.24]")
{
    char msg[120U] = {0};

    /* generate 'u' input data values */
    double uFloat[14] =
    {
        MIN_Q16_16_VALUE,
        -10000.0,
        -100.0,
        -10.5,
        -1.75,
        -0.25,
        0.0,
        0.5,
        3.25,
        10.0,
        105.0,
        2000.0,
        10001.0,
        MAX_Q16_16_VALUE
    };

    /* generate 'v' input data values */
    double vFloat[14] =
    {
        MIN_Q8_24_VALUE,
        -127.0,
        -55.0,
        -10.25,
        -1.5,
        -0.75,
        0.0,
        0.5,
        3.5,
        10.5,
        64.25,
        100.5,
        MAX_Q8_24_VALUE
    };

    for(size_t i = 0U; i < (sizeof(uFloat) / sizeof(uFloat[0])); i++)
    {
        tFixedpoint32 u = test_floatToFixedpoint32(uFloat[i], 16U);
        snprintf(msg, sizeof(msg), "The first input value is %f in Q16.16 fixed-point format", uFloat[i]);
        WHEN(msg)
        {
            for(size_t j = 0U; j < (sizeof(vFloat) / sizeof(vFloat[0])); j++)
            {
                tFixedpoint32 v = test_floatToFixedpoint32(vFloat[j], 24U);
                snprintf(msg, sizeof(msg), "The second input value is %f in Q8.24 fixed-point format", vFloat[j]);
                AND_WHEN(msg)
                {
                    double expectedProductFloat = uFloat[i] * vFloat[j];
                    bool overflow = ((MIN_Q16_16_VALUE > expectedProductFloat) || (MAX_Q16_16_VALUE < expectedProductFloat));
                    if(overflow)
                    {
                        /* arithmetic overflow has occurred */
#if ((defined TEST) && (!defined NDEBUG))
                        snprintf(msg, sizeof(msg), "Multiplying input values %f and %f shall cause assert failure because product is out-of-range", uFloat[i], vFloat[j]);
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                        snprintf(msg, sizeof(msg), "Multiplying input values %f and %f shall cause FIXEDPOINT32_OVERFLOW", uFloat[i], vFloat[j]);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                    }
                    else
                    {
                        snprintf(msg, 100, "The product of input values %f and %f shall be %f in Q16.16 fixed point format",
                                 uFloat[i], vFloat[j], expectedProductFloat);
                    }
                    THEN(msg)
                    {
                        int8_t err = 0xFF;
                        if(overflow)
                        {
#if ((defined TEST) && (!defined NDEBUG))
            REQUIRE_THROWS_WITH( fixedpoint32_multiply(u, v, &err),
                                 Catch::Matchers::StartsWith("assert failed") );
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                            (void) fixedpoint32_multiply(u, v, &err);
                            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                        }
                        else
                        {
                            // UUT
                            tFixedpoint32 product = fixedpoint32_multiply(u, v, &err);
                            REQUIRE_THAT(test_fixedpoint32ToFloat(product), Catch::Matchers::WithinAbs(expectedProductFloat, EPSILON_Q16_16));
                            REQUIRE(FIXEDPOINT32_OK == err);
                            REQUIRE(16U == product.fracLen);
                        }
                    }
                }
            }
        }
    }
}


SCENARIO("Fixed-point multiply: tests for error conditions", "[multiply_errors]")
{

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("u.FracLen is greater than 31")
    {
        const tFixedpoint32 u = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};
        const tFixedpoint32 v = test_floatToFixedpoint32(1.0, 16U);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( fixedpoint32_multiply(u, v, NULL),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("v.FracLen is greater than 31")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(1.0, 16U);
        const tFixedpoint32 v = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( fixedpoint32_multiply(u, v, NULL),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("u is 180.0 and v is 200.0 in Q16.16 fixed-point format")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(180.0, 16U);
        const tFixedpoint32 v = test_floatToFixedpoint32(200.0, 16U);

        THEN("An assert failure shall occur because the product 36000.0 is out-of-range")
        {
            REQUIRE_THROWS_WITH( fixedpoint32_multiply(u, v, NULL),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("u.FracLen is greater than 31")
    {
        const tFixedpoint32 u = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};
        const tFixedpoint32 v = test_floatToFixedpoint32(1.0, 16U);

        THEN("fixedpoint_multiply() shall set error code FIXEDPOINT32_FRAC_LEN_ERROR and return 0.0")
        {
            int8_t err = FIXEDPOINT32_OK;
            double expectedProduct = 0.0;

            // UUT
            tFixedpoint32 product = fixedpoint32_multiply(u, v, &err);
            REQUIRE(FIXEDPOINT32_FRAC_LEN_ERROR == err);
            REQUIRE_THAT(test_fixedpoint32ToFloat(product), Catch::Matchers::WithinAbs(expectedProduct, EPSILON));
        }
    }

    WHEN("v.FracLen is greater than 31")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(1.0, 16U);
        const tFixedpoint32 v = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};

        THEN("fixedpoint_multiply() shall set error code FIXEDPOINT32_FRAC_LEN_ERROR and return 0.0")
        {
            int8_t err = FIXEDPOINT32_OK;
            double expectedProduct = 0.0;

            // UUT
            tFixedpoint32 product = fixedpoint32_multiply(u, v, &err);
            REQUIRE(FIXEDPOINT32_FRAC_LEN_ERROR == err);
            REQUIRE_THAT(test_fixedpoint32ToFloat(product), Catch::Matchers::WithinAbs(expectedProduct, EPSILON));
        }
    }

    WHEN("u is 180.0 and v is 200.0 in Q16.16 fixed-point format")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(180.0, 16U);
        const tFixedpoint32 v = test_floatToFixedpoint32(200.0, 16U);

        THEN("The product 36000.0 is out-of-range and fixedpoint_multiply() shall set error code FIXEDPOINT32_OVERFLOW")
        {
            int8_t err = FIXEDPOINT32_OK;

            // UUT
            (void) fixedpoint32_multiply(u, v, &err);
            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}
