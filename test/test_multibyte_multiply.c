/*
 * test_multibyte_multiply.c - Unit tests: multi-byte multiplication.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include "test.h"
#include "multibyte.h"
#include "catch.hpp"


SCENARIO("Arbitrary length multiply: tests for 32-bit unsigned input data", "[multiplyUint32]")
{
    char msg[100] = {0};
    /* test vectors include values across the entire range of input type uint32_t */
    const uint64_t uTestVector[6] = {0, 63, (MAX_UINT32_VALUE/2), (MAX_UINT32_VALUE/2 + 1), 3456789012, MAX_UINT32_VALUE};
    const uint64_t vTestVector[6] = {0, 64, (MAX_UINT32_VALUE/2), (MAX_UINT32_VALUE/2 + 1), 3456789057, MAX_UINT32_VALUE};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %lu", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %lu", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The product of the input values %lu and %lu shall be %lu",
                             uTestVector[i], vTestVector[j], uTestVector[i] * vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedProduct[8] = UINT64_AS_BYTE_ARRAY(uTestVector[i] * vTestVector[j]);
                        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v));
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
                    }
                }
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t* v = NULL;
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t* product = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer u (in-place multiplication)")
    {
        uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_UNSIGNED(u, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer v (in-place multiplication)")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_UNSIGNED(v, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */
    
    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the product byte array shall be unchanged */
            uint8_t expectedProduct[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t* v = NULL;
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the product byte array shall be unchanged */
            uint8_t expectedProduct[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t* product = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_UNSIGNED(product, u, v, sizeof(u), sizeof(v));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer u (in-place multiplication)")
    {
        uint8_t u[8] = UINT64_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);

        THEN("The result shall be error code MULTIBYTE_OPERATION_NOT_SUPPORTED")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_UNSIGNED(u, u, v, 4, sizeof(v));
            REQUIRE(MULTIBYTE_OPERATION_NOT_SUPPORTED == result);
            /* the product byte array (u) shall be unchanged */
            uint8_t expectedProduct[8] = UINT64_AS_BYTE_ARRAY(1234U);
            REQUIRE(test_byteArraysAreEqual(expectedProduct, u, sizeof(expectedProduct)));
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer v (in-place multiplication)")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        uint8_t v[8] = UINT64_AS_BYTE_ARRAY(5678U);

        THEN("The result shall be error code MULTIBYTE_OPERATION_NOT_SUPPORTED")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_UNSIGNED(v, u, v, sizeof(u), 4);
            REQUIRE(MULTIBYTE_OPERATION_NOT_SUPPORTED == result);
            /* the product byte array (v) shall be unchanged */
            uint8_t expectedProduct[8] = UINT64_AS_BYTE_ARRAY(5678U);
            REQUIRE(test_byteArraysAreEqual(expectedProduct, v, sizeof(expectedProduct)));
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Arbitrary length multiply: tests for 32-bit signed input data", "[multiplyInt32]")
{
    char msg[100] = {0};
    /* test vectors include values across the entire range of input type int32_t */
    const int64_t uTestVector[6] = {MIN_INT32_VALUE, (MIN_INT32_VALUE/2), -1, 0, (MAX_INT32_VALUE/2), MAX_INT32_VALUE};
    const int64_t vTestVector[6] = {MIN_INT32_VALUE, (MIN_INT32_VALUE/2 + 7), 0, 1, (MAX_INT32_VALUE/2 + 7), MAX_INT32_VALUE};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %ld", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[4] = INT32_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %ld", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = INT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The product of the input values %ld and %ld shall be %ld",
                             uTestVector[i], vTestVector[j], uTestVector[i] * vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedProduct[8] = INT64_AS_BYTE_ARRAY(uTestVector[i] * vTestVector[j]);
                        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v));
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
                    }
                }
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t* v = NULL;
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);
        uint8_t* product = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer u (in-place multiplication)")
    {
        uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_SIGNED(u, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer v (in-place multiplication)")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_MULTIPLY_SIGNED(v, u, v, sizeof(u), sizeof(v)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the product byte array shall be unchanged */
            uint8_t expectedProduct[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t* v = NULL;
        uint8_t product[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the product byte array shall be unchanged */
            uint8_t expectedProduct[8] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);
        uint8_t* product = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer u (in-place multiplication)")
    {
        uint8_t u[8] = INT64_AS_BYTE_ARRAY(1234);
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("The result shall be error code MULTIBYTE_OPERATION_NOT_SUPPORTED")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_SIGNED(u, u, v, 4, sizeof(v));
            REQUIRE(MULTIBYTE_OPERATION_NOT_SUPPORTED == result);
            /* the product byte array (u) shall be unchanged */
            uint8_t expectedProduct[8] = INT64_AS_BYTE_ARRAY(1234U);
            REQUIRE(test_byteArraysAreEqual(expectedProduct, u, sizeof(expectedProduct)));
        }
    }

    WHEN("Output parameter pointer w is the same as input parameter pointer v (in-place multiplication)")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        uint8_t v[8] = INT64_AS_BYTE_ARRAY(-5678);

        THEN("The result shall be error code MULTIBYTE_OPERATION_NOT_SUPPORTED")
        {
            // UUT
            int8_t result = MULTIBYTE_MULTIPLY_SIGNED(v, u, v, sizeof(u), 4);
            REQUIRE(MULTIBYTE_OPERATION_NOT_SUPPORTED == result);
            /* the product byte array (v) shall be unchanged */
            uint8_t expectedProduct[8] = INT64_AS_BYTE_ARRAY(-5678);
            REQUIRE(test_byteArraysAreEqual(expectedProduct, v, sizeof(expectedProduct)));
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Arbitrary length multiply: tests for 16-bit and 32-bit signed input data", "[multiplyInt16Int32]")
{
    char msg[100] = {0};
    /* test vectors include values across the entire range of input types int16_t and int32_t */
    const int32_t uTestVector[6] = {MIN_INT16_VALUE, (MIN_INT16_VALUE/2), -1, 0, (MAX_INT16_VALUE/2), MAX_INT16_VALUE};
    const int64_t vTestVector[6] = {MIN_INT32_VALUE, (MIN_INT32_VALUE/2 + 7), 0, 1, (MAX_INT32_VALUE/2 + 7), MAX_INT32_VALUE};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %d", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[2] = INT16_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %ld", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = INT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The product of the input values %d and %ld shall be %ld",
                             uTestVector[i], vTestVector[j], uTestVector[i] * vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedProduct[6] = INT48_AS_BYTE_ARRAY(uTestVector[i] * vTestVector[j]);
                        uint8_t product[6] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_MULTIPLY_SIGNED(product, u, v, sizeof(u), sizeof(v));
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedProduct, product, sizeof(expectedProduct)));
                    }
                }
            }
        }
    }
}
