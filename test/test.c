/*
 * test.c - Unit tests: Helper functions source file.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include "test.h"
#include "catch.hpp"
#include <math.h>


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

bool test_byteArraysAreEqual(const uint8_t expected[], const uint8_t actual[], const size_t length)
{
    bool result = true;
    for(size_t i = 0; i < length; i++)
    {
        if(expected[i] != actual[i])
        {
            result = false;
            break;
        }
    }
    return result;
}


tFixedpoint32 test_floatToFixedpoint32(const double value, const int8_t fracLen)
{
    /* floating point value shall be within the range of the specified fixed-point format */
    double upperLimit = (pow(2.0, 31) - 1) / pow(2.0, fracLen);
    double lowerLimit = - pow(2.0, 31) / pow(2.0, fracLen);
    REQUIRE(upperLimit >= value);
    REQUIRE(lowerLimit <= value);

    /* Create and initialise the fixed-point struct */
    tFixedpoint32 fp;
    fp.fracLen = fracLen;
    int32_t intValue = (int32_t) (value * pow(2.0, fracLen));
    /* cast to unsigned integer to avoid undefined behaviour of bitwise operator and bit-shifting */
    uint32_t uintValue = (uint32_t) intValue;
    for(uint8_t i = 0; i < 4; i++)
    {
        fp.data[i] = (uint8_t) (uintValue & 0xFFU);
        uintValue = uintValue >> 8U;
    }
    return fp;
}


double test_fixedpoint32ToFloat(const tFixedpoint32 fp)
{
    /* perform bit shifting operations as unsigned integer to avoid undefined behaviour */
    uint32_t uintValue = 0;
    for(int8_t i = 3; i >= 0; i--)
    {
        uintValue = uintValue << 8U;
        uintValue += fp.data[i];
    }
    int32_t intValue = (int32_t) uintValue;
    double floatValue = (double) intValue / pow(2.0, fp.fracLen);
    return floatValue;
}
