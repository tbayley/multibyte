/*
 * test.h - Unit tests: Helper functions and macros header file.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef TEST_H
#define TEST_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "fixedpoint32.h"


/* PUBLIC MACROS *************************************************************/

/*! Maximum difference for floating point equality test */
#define EPSILON 1E-6

#define MAX_UINT8_VALUE   255U          //!< Maximum value of uint8_t integer.
#define MIN_UINT8_VALUE   0U            //!< Minimum value of uint8_t integer.
#define MAX_INT8_VALUE    127           //!< Maximum value of int8_t integer.
#define MIN_INT8_VALUE   -128           //!< Minimum value of int8_t integer.

#define MAX_UINT16_VALUE  65535U        //!< Maximum value of uint16_t integer.
#define MIN_UINT16_VALUE  0U            //!< Minimum value of uint16_t integer.
#define MAX_INT16_VALUE   32767         //!< Maximum value of int16_t integer.
#define MIN_INT16_VALUE  -32768         //!< Minimum value of int16_t integer.

#define MAX_UINT32_VALUE  4294967295U   //!< Maximum value of uint32_t integer.
#define MIN_UINT32_VALUE  0U            //!< Minimum value of uint32_t integer.
#define MAX_INT32_VALUE   2147483647    //!< Maximum value of int32_t integer.
#define MIN_INT32_VALUE  -2147483648    //!< Minimum value of int32_t integer.

#define Q16_16_LSB_VALUE  (1.0 / 65536.0)                       //!< Least-significant bit value for Q16.16 fixed-point format.
#define MAX_Q16_16_VALUE  (Q16_16_LSB_VALUE * MAX_INT32_VALUE)  //!< Maximum value of Q16.16 fixed-point value.
#define MIN_Q16_16_VALUE  (Q16_16_LSB_VALUE * MIN_INT32_VALUE)  //!< Minimum value of Q16.16 fixed-point value.

#define Q8_24_LSB_VALUE  (1.0 / 16777216.0)                     //!< Least-significant bit value for Q8.24 fixed-point format.
#define MAX_Q8_24_VALUE  (Q8_24_LSB_VALUE * MAX_INT32_VALUE)    //!< Maximum value of Q8.24 fixed-point value.
#define MIN_Q8_24_VALUE  (Q8_24_LSB_VALUE * MIN_INT32_VALUE)    //!< Minimum value of Q8.24 fixed-point value.


/*! Convert unsigned 16-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Unsigned 16-bit value.
 *  @return         Unsigned byte array of length 2 bytes.
 */
#define UINT16_AS_BYTE_ARRAY(value) {                 \
    (uint8_t) (((uint16_t) (value)) & 0xFFU),         \
    (uint8_t) ((((uint16_t) (value)) >> 8) & 0xFFU)   \
}


/*! Convert signed 16-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Signed 16-bit value.
 *  @return         Unsigned byte array of length 2 bytes.
 */
#define INT16_AS_BYTE_ARRAY(value)  UINT16_AS_BYTE_ARRAY(((uint16_t)(value)))


/*! Convert unsigned 32-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Unsigned 32-bit value.
 *  @return         Unsigned byte array of length 4 bytes.
 */
#define UINT32_AS_BYTE_ARRAY(value) {                 \
    (uint8_t) (((uint32_t) (value)) & 0xFFU),         \
    (uint8_t) ((((uint32_t) (value)) >> 8) & 0xFFU),  \
    (uint8_t) ((((uint32_t) (value)) >> 16) & 0xFFU), \
    (uint8_t) ((((uint32_t) (value)) >> 24) & 0xFFU)  \
}


/*! Convert signed 32-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Signed 32-bit value.
 *  @return         Unsigned byte array of length 4 bytes.
 */
#define INT32_AS_BYTE_ARRAY(value)  UINT32_AS_BYTE_ARRAY(((uint32_t)(value)))


/*! Convert unsigned 40-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Unsigned 40-bit value.
 *  @return         Unsigned byte array of length 5 bytes.
 */
#define UINT40_AS_BYTE_ARRAY(value) {                   \
    (uint8_t) (((uint64_t) (value)) & 0xFFUL),          \
    (uint8_t) ((((uint64_t) (value)) >> 8) & 0xFFUL),   \
    (uint8_t) ((((uint64_t) (value)) >> 16) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 24) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 32) & 0xFFUL),  \
}


/*! Convert Signed 40-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Signed 40-bit value.
 *  @return         Unsigned byte array of length 5 bytes.
 */
#define INT40_AS_BYTE_ARRAY(value)  UINT40_AS_BYTE_ARRAY(((uint64_t)(value)))


/*! Convert unsigned 48-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Unsigned 48-bit value.
 *  @return         Unsigned byte array of length 8 bytes.
 */
#define UINT48_AS_BYTE_ARRAY(value) {                   \
    (uint8_t) (((uint64_t) (value)) & 0xFFUL),          \
    (uint8_t) ((((uint64_t) (value)) >> 8) & 0xFFUL),   \
    (uint8_t) ((((uint64_t) (value)) >> 16) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 24) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 32) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 40) & 0xFFUL)   \
}


/*! Convert Signed 48-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Signed 48-bit value.
 *  @return         Unsigned byte array of length 6 bytes.
 */
#define INT48_AS_BYTE_ARRAY(value)  UINT48_AS_BYTE_ARRAY(((uint64_t)(value)))


/*! Convert unsigned 64-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Unsigned 64-bit value.
 *  @return         Unsigned byte array of length 8 bytes.
 */
#define UINT64_AS_BYTE_ARRAY(value) {                   \
    (uint8_t) (((uint64_t) (value)) & 0xFFUL),          \
    (uint8_t) ((((uint64_t) (value)) >> 8) & 0xFFUL),   \
    (uint8_t) ((((uint64_t) (value)) >> 16) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 24) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 32) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 40) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 48) & 0xFFUL),  \
    (uint8_t) ((((uint64_t) (value)) >> 56) & 0xFFUL)   \
}


/*! Convert Signed 64-bit value to a byte array with little-endian byte order.
 *
 *  @param  value   Signed 64-bit value.
 *  @return         Unsigned byte array of length 8 bytes.
 */
#define INT64_AS_BYTE_ARRAY(value)  UINT64_AS_BYTE_ARRAY(((uint64_t)(value)))


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 * Test if two byte arrays are equal.
 * 
 * @param[in]  expected    The expected byte array.
 * @param[in]  actual      The actual byte array.
 * @param      length      Length of the byte arrays, in bytes.
 * 
 * @return                 True if actual[] == expected[], else false.
 */
bool test_byteArraysAreEqual(const uint8_t expected[], const uint8_t actual[], const size_t length);


/*
 * Create a 32-bit signed fixed-point object and initialise from a floating point value.
 *
 * @param   value       Floating point value that will initialise the fixed-point object.
 * @param   fracLen     Number of fraction bits.
 *
 * @return              Fixed-point object.
 */
tFixedpoint32 test_floatToFixedpoint32(const double value, const int8_t fracLen);


/*!
 * Convert fixed-point to floating point
 *
 * @param   fp  Fixed-point object.
 * @return      Floating-point value.
 */
double test_fixedpoint32ToFloat(const tFixedpoint32 fp);


#ifdef __cplusplus
}
#endif

#endif  /* TEST_H */
