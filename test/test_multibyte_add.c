/*
 * test_multibyte_add.c - Unit tests: multi-byte addition.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include "test.h"
#include "multibyte.h"
#include "catch.hpp"


/* Change next line to '#define EXTENDED_TEST' to run exhaustive addition tests.
 * These tests take a long time to execute, so are not enabled by default.
 */
#undef EXTENDED_TEST


SCENARIO("Addition, 8-bit unsigned input data: growLen == true", "[addUint8]")
{
    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    /* test vectors include values across the entire range of input type uint8_t */
    const uint16_t uTestVector[9] = {0, 1, 63, 127, 128, 190, 213, 254, 255};
    const uint16_t vTestVector[9] = {0, 1, 64, 127, 128, 180, 207, 254, 255};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %d", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[1] = {(uint8_t) uTestVector[i]};  // first input value
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %d", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[1] = {(uint8_t) vTestVector[j]};  // second input value
                    snprintf(msg, 100, "The sum of the input values %d and %d shall be %d",
                             uTestVector[i], vTestVector[j], uTestVector[i] + vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[2] = UINT16_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        uint8_t sum[2] = {0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

SCENARIO("Addition, 8-bit signed input data: growLen == true", "[addInt8]")
{
    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    /* test vectors include values across the entire range of input type int8_t */
    const int16_t uTestVector[9] = {-128, -127, -63, -1, 0, 1, 64, 126, 127};
    const int16_t vTestVector[9] = {-128, -127, -65, -1, 0, 1, 62, 126, 127};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %d", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[1] = {(uint8_t) uTestVector[i]};  // first input value
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %d", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[1] = {(uint8_t) vTestVector[j]};  // second input value
                    snprintf(msg, 100, "The sum of the input values %d and %d shall be %d",
                             uTestVector[i], vTestVector[j], uTestVector[i] + vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[2] = INT16_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        uint8_t sum[2] = {0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_ADD_SIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

SCENARIO("Addition in-place, 8-bit signed input data: growLen == true", "[addInt8InPlace]")
{
    /* For these tests the sum overwrites the input data in array 'u' */

    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    /* test vectors include values across the entire range of input type int8_t */
    const int16_t uTestVector[9] = {-128, -127, -63, -1, 0, 1, 64, 126, 127};
    const int16_t vTestVector[9] = {-128, -127, -65, -1, 0, 1, 62, 126, 127};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %d", uTestVector[i]);
        WHEN(msg)
        {
            uint8_t u[2] = {(uint8_t) uTestVector[i], 0xAA};  // first single-byte input value is in LSByte, MSByte is initialised with garbage
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %d", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[1] = {(uint8_t) vTestVector[j]};  // second single-byte input value
                    snprintf(msg, 100, "The sum of the input values %d and %d shall be %d",
                             uTestVector[i], vTestVector[j], uTestVector[i] + vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[2] = INT16_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        // UUT
                        int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, 1, 1, growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, u, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

SCENARIO("Addition, 32-bit unsigned input data: growLen == true", "[addUint32]")
{
    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    /* test vectors include values across the entire range of input type uint32_t */
    const uint64_t uTestVector[8] = {0, 1, 63, (MAX_UINT32_VALUE/2), (MAX_UINT32_VALUE/2 + 1), 3456789012, MAX_UINT32_VALUE - 1, MAX_UINT32_VALUE};
    const uint64_t vTestVector[8] = {0, 1, 64, (MAX_UINT32_VALUE/2), (MAX_UINT32_VALUE/2 + 1), 3456789057, MAX_UINT32_VALUE - 1, MAX_UINT32_VALUE};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %lu", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %lu", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The sum of the input values %lu and %lu shall be %lu",
                             uTestVector[i], vTestVector[j], uTestVector[i] + vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[5] = UINT40_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        uint8_t sum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t sum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            // UUT
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t* v = NULL;
        uint8_t sum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            // UUT
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t* sum = NULL;

        THEN("An assert failure shall occur")
        {
            // UUT
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t sum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedSum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t* v = NULL;
        uint8_t sum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedSum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t* sum = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Addition in-place tests for 32-bit signed input data: growLen == true", "[addInt32InPlace]")
{
    /* For these tests the sum overwrites the input data in array 'u' */

    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    /* test vectors include values across the entire range of input type int32_t */
    const int64_t uTestVector[8] = {MIN_INT32_VALUE, (MIN_INT32_VALUE + 1), (MIN_INT32_VALUE/2), -1, 0, (MAX_INT32_VALUE/2), (MAX_INT32_VALUE - 1), MAX_INT32_VALUE};
    const int64_t vTestVector[8] = {MIN_INT32_VALUE, (MIN_INT32_VALUE + 1), (MIN_INT32_VALUE/2 + 7), 0, 1, (MAX_INT32_VALUE/2 + 7), (MAX_INT32_VALUE - 1), MAX_INT32_VALUE};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %ld", uTestVector[i]);
        WHEN(msg)
        {
            uint8_t u[5] = INT32_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %ld", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = INT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The sum of the input values %ld and %ld shall be %ld",
                             uTestVector[i], vTestVector[j], uTestVector[i] + vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[5] = INT40_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, 4, 4, growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, u, sizeof(expectedSum)));
                    }
                }
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        uint8_t* u = NULL;
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_SIGNED(u, u, v, 4, sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        uint8_t u[5] = INT40_AS_BYTE_ARRAY(1234);
        const uint8_t* v = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_SIGNED(u, u, v, 4, 4, growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        uint8_t* u = NULL;
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, 4, sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        uint8_t u[5] = INT40_AS_BYTE_ARRAY(1234);
        const uint8_t* v = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, 4, 4, growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedU[5] = UINT32_AS_BYTE_ARRAY(1234U);
            REQUIRE(test_byteArraysAreEqual(expectedU, u, sizeof(expectedU)));
        }
    }


#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Addition, 16-bit and 32-bit signed input data: growLen == true", "[addInt16Int32]")
{
    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    /* test vectors include values across the entire range of input types int16_t and int32_t */
    const int32_t uTestVector[8] = {MIN_INT16_VALUE, (MIN_INT16_VALUE + 1), (MIN_INT16_VALUE/2), -1, 0, (MAX_INT16_VALUE/2), (MAX_INT16_VALUE - 1), MAX_INT16_VALUE};
    const int64_t vTestVector[8] = {MIN_INT32_VALUE, (MIN_INT16_VALUE + 1), (MIN_INT32_VALUE/2 + 7), 0, 1, (MAX_INT32_VALUE/2 + 7), (MAX_INT16_VALUE - 1), MAX_INT32_VALUE};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %d", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[2] = INT16_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %ld", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = INT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The sum of the input values %d and %ld shall be %ld",
                             uTestVector[i], vTestVector[j], uTestVector[i] + vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[5] = INT40_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        uint8_t sum[5] = {0xAA, 0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        int8_t result = MULTIBYTE_ADD_SIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

SCENARIO("Addition, 32-bit unsigned input data: growLen == false", "[addUint32_growLenFalse]")
{
    /* output word length is equal to input word length, arithmetic overflow can occur */
    const bool growLen = false;

    /* test vectors include values across the entire range of input type uint32_t */
    const uint64_t uTestVector[8] = {0, 1, 63, (MAX_UINT32_VALUE/2 + 1), (MAX_UINT32_VALUE/2 + 2), 3456789012, MAX_UINT32_VALUE - 1, MAX_UINT32_VALUE};
    const uint64_t vTestVector[8] = {0, 1, 64, (MAX_UINT32_VALUE/2 + 1), (MAX_UINT32_VALUE/2 + 2), 3456789057, MAX_UINT32_VALUE - 1, MAX_UINT32_VALUE};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %lu", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %lu", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The sum of the input values %lu and %lu shall be %u",
                             uTestVector[i], vTestVector[j], (uint32_t) uTestVector[i] + (uint32_t) vTestVector[j]);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[4] = UINT32_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        const bool expectedOverflow = (uTestVector[i] + vTestVector[j] > MAX_UINT32_VALUE);
                        const int8_t expectedResult = expectedOverflow ? MULTIBYTE_OVERFLOW : MULTIBYTE_OK;
                        uint8_t sum[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        // UUT
                        int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(expectedResult == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t sum[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_UNSIGNED(sum, u, v, 4, sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t* v = NULL;
        uint8_t sum[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), 4, growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t* sum = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t sum[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, 4, sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedSum[4] = {0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t* v = NULL;
        uint8_t sum[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), 4, growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedSum[4] = {0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t v[4] = UINT32_AS_BYTE_ARRAY(5678U);
        uint8_t* sum = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Addition in-place, 32-bit signed input data: growLen == false", "[addInt32InPlace_growLenFalse]")
{
    /* For these tests the sum overwrites the input data in array 'u' */

    /* output word length is equal to input word length, arithmetic overflow can occur */
    const bool growLen = false;

    /* test vectors include values across the entire range of input type int32_t */
    const int64_t uTestVector[8] = {MIN_INT32_VALUE, (MIN_INT32_VALUE + 1), (MIN_INT32_VALUE/2), -1, 0, (MAX_INT32_VALUE/2), (MAX_INT32_VALUE - 1), MAX_INT32_VALUE};
    const int64_t vTestVector[8] = {MIN_INT32_VALUE, (MIN_INT32_VALUE + 1), (MIN_INT32_VALUE/2 + 7), 0, 1, (MAX_INT32_VALUE/2 + 7), (MAX_INT32_VALUE - 1), MAX_INT32_VALUE};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %ld", uTestVector[i]);
        WHEN(msg)
        {
            uint8_t u[4] = INT32_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %ld", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = INT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The sum of the input values %ld and %ld shall be %ld",
                             uTestVector[i], vTestVector[j], (uTestVector[i] + vTestVector[j]) & 0xFFFFFFFF);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[4] = INT32_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        const bool expectedOverflow = ((uTestVector[i] + vTestVector[j] > MAX_INT32_VALUE) ||
                                                       (uTestVector[i] + vTestVector[j] < MIN_INT32_VALUE));
                        const int8_t expectedResult = expectedOverflow ? MULTIBYTE_OVERFLOW : MULTIBYTE_OK;
                        // UUT
                        int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, 4, 4, growLen);
                        REQUIRE(expectedResult == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, u, sizeof(expectedSum)));
                    }
                }
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        uint8_t* u = NULL;
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_SIGNED(u, u, v, sizeof(u), sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t* v = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_ADD_SIGNED(u, u, v, sizeof(u), sizeof(v), growLen),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        uint8_t* u = NULL;
        const uint8_t v[4] = INT32_AS_BYTE_ARRAY(-5678);

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, sizeof(u), sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

    WHEN("Input parameter pointer v is NULL")
    {
        uint8_t u[4] = INT32_AS_BYTE_ARRAY(1234);
        const uint8_t* v = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_ADD_SIGNED(u, u, v, sizeof(u), sizeof(v), growLen);
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedU[4] = INT32_AS_BYTE_ARRAY(1234);
            REQUIRE(test_byteArraysAreEqual(expectedU, u, sizeof(expectedU)));
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Addition, 16-bit and 32-bit signed input data: growLen == false", "[addInt16Int32_growLenFalse]")
{
    /* output word length is equal to input word length, arithmetic overflow can occur */
    const bool growLen = false;

    /* test vectors include values across the entire range of input types int16_t and int32_t */
    const int32_t uTestVector[8] = {MIN_INT16_VALUE, (MIN_INT16_VALUE + 1), (MIN_INT16_VALUE/2), -1, 0, (MAX_INT16_VALUE/2), (MAX_INT16_VALUE - 1), MAX_INT16_VALUE};
    const int64_t vTestVector[8] = {MIN_INT32_VALUE, (MIN_INT16_VALUE + 1), (MIN_INT32_VALUE/2 + 7), 0, 1, (MAX_INT32_VALUE/2 + 7), (MAX_INT16_VALUE - 1), MAX_INT32_VALUE};

    char msg[100] = {0};

    for(size_t i = 0; i < sizeof(uTestVector) / sizeof(uTestVector[0]); i++)
    {
        snprintf(msg, 100, "The first input value is %d", uTestVector[i]);
        WHEN(msg)
        {
            const uint8_t u[2] = INT16_AS_BYTE_ARRAY(uTestVector[i]);
            for(size_t j = 0; j < sizeof(vTestVector) / sizeof(vTestVector[0]); j++)
            {
                snprintf(msg, 100, "The second input value is %ld", vTestVector[j]);
                AND_WHEN(msg)
                {
                    const uint8_t v[4] = INT32_AS_BYTE_ARRAY(vTestVector[j]);
                    snprintf(msg, 100, "The sum of the input values %d and %ld shall be %ld",
                             uTestVector[i], vTestVector[j], (uTestVector[i] + vTestVector[j]) & 0xFFFFFFFF);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[4] = INT32_AS_BYTE_ARRAY(uTestVector[i] + vTestVector[j]);
                        const bool expectedOverflow = ((uTestVector[i] + vTestVector[j] > MAX_INT32_VALUE) ||
                                                       (uTestVector[i] + vTestVector[j] < MIN_INT32_VALUE));
                        const int8_t expectedResult = expectedOverflow ? MULTIBYTE_OVERFLOW : MULTIBYTE_OK;
                        uint8_t sum[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                        int8_t result = MULTIBYTE_ADD_SIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(expectedResult == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

#ifdef EXTENDED_TEST

/* The EXTENDED_TEST scenarios take a very long time to build and run! */

SCENARIO("Addition extended tests for 8-bit signed input data: growLen == true", "[addInt8_extended]")
{
    /* This scenario exhaustively tests the addition of every possible
     * combination of two int8_t byte values.
     */

    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    char msg[100] = {0};

    for(int32_t uIndex = -128; uIndex < 128; uIndex++)
    {
        snprintf(msg, 100, "The first input value is %d", uIndex);
        WHEN(msg)
        {
            const uint8_t u[1] = {(uint8_t) uIndex};    // first input value
            for(int32_t vIndex = -128; vIndex < 128; vIndex++)
            {
                snprintf(msg, 100, "The second input value is %d", vIndex);
                AND_WHEN(msg)
                {
                    const uint8_t v[1] = {(uint8_t) vIndex};    // second input value

                    snprintf(msg, 100, "The sum of the input values %d and %d shall be %d", uIndex, vIndex, uIndex + vIndex);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[2] = INT16_AS_BYTE_ARRAY(uIndex + vIndex);
                        uint8_t sum[2] = {0xAA, 0xAA};    // initialise the output array with garbage
                        int8_t result = MULTIBYTE_ADD_SIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

SCENARIO("Addition extended tests for 8-bit unsigned input data: growLen == true", "[addUint8_extended]")
{
    /* This scenario exhaustively tests the addition of every possible
     * combination of two uint8_t byte values.
     */

    /* word length may grow, arithmetic overflow cannot occur */
    const bool growLen = true;

    char msg[100] = {0};

    for(uint32_t uIndex = 0; uIndex < 256; uIndex++)
    {
        snprintf(msg, 100, "The first input value is %u", uIndex);
        WHEN(msg)
        {
            const uint8_t u[1] = {(uint8_t) uIndex};    // first input value
            for(uint32_t vIndex = 0; vIndex < 256; vIndex++)
            {
                snprintf(msg, 100, "The second input value is %u", vIndex);
                AND_WHEN(msg)
                {
                    const uint8_t v[1] = {(uint8_t) vIndex};    // second input value

                    snprintf(msg, 100, "The sum of the input values %u and %u shall be %u", uIndex, vIndex, uIndex + vIndex);
                    THEN(msg)
                    {
                        const uint8_t expectedSum[2] = UINT16_AS_BYTE_ARRAY(uIndex + vIndex);
                        uint8_t sum[2] = {0xAA, 0xAA};    // initialise the output array with garbage
                        int8_t result = MULTIBYTE_ADD_UNSIGNED(sum, u, v, sizeof(u), sizeof(v), growLen);
                        REQUIRE(MULTIBYTE_OK == result);
                        REQUIRE(test_byteArraysAreEqual(expectedSum, sum, sizeof(expectedSum)));
                    }
                }
            }
        }
    }
}

#endif  /* #ifdef EXTENDED_TEST */
