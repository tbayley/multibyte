# Multibyte integer and fixed-point arithmetic: Unit tests #


## Introduction ##

This directory contains unit tests for the multibyte integer and fixed-point
arithmetic functions, whose source code is in the _multibyte/source_
subdirectory.

The unit tests use the [Catch2](https://github.com/catchorg/Catch2) unit test
framework and [Fake Function Framework](https://github.com/meekrosoft/fff)
mocking framework.  These are both C++ header-only frameworks, with no
dependencies on other software packages.


## Installing the prerequisites ##

Although the multibyte integer and fixed-point arithmetic functions are written
in C, the unit test and mocking frameworks are C++. Therefore all source files
must be built using a C++ compiler. The unit tests are intended to be built and
run using the GNU g++ compiler on a Linux machine, or on
[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps).
Tests are built using the [SCons](https://scons.org/) build system. Test
coverage reports are generated using the Python package [GCOVR](https://gcovr.com/).

The GNU toolchain is pre-installed on many Linux distributions. The unit tests
use GCC [Sanitizer](https://gcc.gnu.org/onlinedocs/gcc-9.3.0/gcc/Instrumentation-Options.html#Instrumentation-Options)
options to check for addressing faults and undefined-behaviour at run-time. The
sanitizers require Linux libraries _libasan_ and _libubsan_ to be installed. If
you need to install any of these packages, use your system's package manager
utility. On Ubuntu Linux the relevant command would be:

```bash
sudo apt install build-essential libasan5 libubsan0 libubsan1
```

[SCons](https://scons.org/) and [GCOVR](https://gcovr.com/) are Python packages
that are installed from the Python Package Index (PyPi). First install the
latest version of [Python](https://www.python.org/downloads/). Python 3.5 or
later is recommended. Then use _pip_ to install SCons and GCOVR:

```bash
pip install scons
pip install gcovr
```


## Building and running unit tests ##

To build and run all unit tests, open a terminal session in the _multibyte_
project's root directory and enter the following command:

```bash
scons coverage
```

This command builds and runs the unit tests, writing pass/fail results to the
command line terminal. HTML test coverage reports are generated for each file
and a summary report is created at _multibyte/build/coverage-reports/coverage.html_.
Unit tests can be run without coverage reporting by executing the command:

```bash
scons run
```

If the command line option _--xmlreport_ is used, the test runner writes
Junit-style XML test reports to subdirectory _multibyte/build/test-reports_
instead of writing human-readable text pass/fail reports to the console.

```bash
scons coverage --xmlreport
```

If SCons is executed with no build target specified, it defaults to building all
targets - including _coverage_. 

To delete the build artefacts, enter the command:

```bash
scons --clean
```

You can also build and run a single test_XXX.c file (for example
test_multibyte_add.c) by executing the the commands:

```bash
scons test_XXX
build/test/test_XXX
```

The [Visual Studio Code](https://code.visualstudio.com/) workspace
_multibyte.code-workspace_, in the project's root directory, enables unit tests
to be built, run and debugged via _Visual Studio Code_ menus.  For example, to
build and run the unit tests defined in the source file _test\_multibyte.c_
from within _Visual Studio Code_, open the file _test/test\_multibyte.c_ in the
editor, then select the menu item
_Terminal > Run Task... > run selected unit test file_.
