/*
 * test_fixedpoint32_subtract.c - Unit tests: 32-bit signed fixed-point subtraction.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <math.h>
#include "test.h"
#include "fixedpoint32.h"
#include "catch.hpp"


/* PUBLIC MACROS *************************************************************/

/*! Maximum difference for equality test when testing Q16.16 fixed-point values */
#define EPSILON_Q16_16 (2.0 * (1.0 / 65536.0))


/* UNIT TEST DEFINITIONS *****************************************************/

SCENARIO("Fixed-point subtract: tests for Q16.16 input data", "[subtract_Q16.16]")
{
    char msg[120U] = {0};
    /* generate 10 equally spaced 'u' input data values and 9 'v' values */
    size_t uSteps = 10U;
    double uIncrement = (MAX_Q16_16_VALUE - MIN_Q16_16_VALUE) / (uSteps - 1U);
    size_t vSteps = 9U;
    double vIncrement = 3.0 + (MAX_Q16_16_VALUE - MIN_Q16_16_VALUE) / (uSteps - 1U);

    for(size_t i = 0U; i < uSteps; i++)
    {
        double uFloat = MIN_Q16_16_VALUE + (uIncrement * i);
        tFixedpoint32 u = test_floatToFixedpoint32(uFloat, 16U);
        snprintf(msg, sizeof(msg), "The first input value is %f", uFloat);
        WHEN(msg)
        {
            for(size_t j = 0U; j < vSteps; j++)
            {
                double vFloat = MIN_Q16_16_VALUE + (vIncrement * j);
                tFixedpoint32 v = test_floatToFixedpoint32(vFloat, 16U);
                snprintf(msg, sizeof(msg), "The second input value is %f", vFloat);
                AND_WHEN(msg)
                {
                    double expectedDiffFloat = uFloat - vFloat;
                    bool overflow = ((MIN_Q16_16_VALUE > expectedDiffFloat) || (MAX_Q16_16_VALUE < expectedDiffFloat));
                    if(overflow)
                    {
                        /* arithmetic overflow has occurred */
#if ((defined TEST) && (!defined NDEBUG))
                        snprintf(msg, sizeof(msg), "Subtracting input value %f from %f shall cause assert failure because difference is out of range", vFloat, uFloat);
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                        snprintf(msg, sizeof(msg), "Subtracting input value %f from %f shall cause FIXEDPOINT32_OVERFLOW", vFloat, uFloat);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                    }
                    else
                    {
                        snprintf(msg, 100, "The difference of input values %f and %f shall be %f",
                                 uFloat, vFloat, expectedDiffFloat);
                    }
                    THEN(msg)
                    {
                        int8_t err = 0xFF;
                        if(overflow)
                        {
#if ((defined TEST) && (!defined NDEBUG))
            REQUIRE_THROWS_WITH( fixedpoint32_subtract(u, v, &err),
                                 Catch::Matchers::StartsWith("assert failed") );
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                            (void) fixedpoint32_subtract(u, v, &err);
                            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                        }
                        else
                        {
                            // UUT
                            tFixedpoint32 diff = fixedpoint32_subtract(u, v, &err);
                            REQUIRE_THAT(test_fixedpoint32ToFloat(diff), Catch::Matchers::WithinAbs(expectedDiffFloat, EPSILON_Q16_16));
                            REQUIRE(FIXEDPOINT32_OK == err);
                        }
                    }
                }
            }
        }
    }
}


SCENARIO("Fixed-point subtract: tests for Q16.16 and Q8.24 input data", "[subtract_Q16.16_and_Q8.24]")
{
    char msg[120U] = {0};
    /* generate 10 equally spaced 'u' input data values and 9 'v' values */
    size_t uSteps = 10U;
    double uIncrement = (MAX_Q16_16_VALUE - MIN_Q16_16_VALUE) / (uSteps - 1U);
    size_t vSteps = 9U;
    double vIncrement = 1.0 + (MAX_Q8_24_VALUE - MIN_Q8_24_VALUE) / (uSteps - 1U);

    for(size_t i = 0U; i < uSteps; i++)
    {
        double uFloat = MIN_Q16_16_VALUE + (uIncrement * i);
        tFixedpoint32 u = test_floatToFixedpoint32(uFloat, 16U);
        snprintf(msg, sizeof(msg), "The first input value is %f in Q16.16 fixed-point format", uFloat);
        WHEN(msg)
        {
            for(size_t j = 0U; j < vSteps; j++)
            {
                double vFloat = MIN_Q8_24_VALUE + (vIncrement * j);
                tFixedpoint32 v = test_floatToFixedpoint32(vFloat, 24U);
                snprintf(msg, sizeof(msg), "The second input value is %f in Q8.24 fixed-point format", vFloat);
                AND_WHEN(msg)
                {
                    double expectedDiffFloat = uFloat - vFloat;
                    bool overflow = ((MIN_Q16_16_VALUE > expectedDiffFloat) || (MAX_Q16_16_VALUE < expectedDiffFloat));
                    if(overflow)
                    {
                        /* arithmetic overflow has occurred */
#if ((defined TEST) && (!defined NDEBUG))
                        snprintf(msg, sizeof(msg), "Subtracting input value %f from %f shall cause assert failure because difference is out of range", vFloat, uFloat);
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                        snprintf(msg, sizeof(msg), "Subtracting input value %f from %f shall cause FIXEDPOINT32_OVERFLOW", vFloat, uFloat);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                    }
                    else
                    {
                        snprintf(msg, 100, "The difference of input values %f and %f shall be %f",
                                 uFloat, vFloat, expectedDiffFloat);
                    }
                    THEN(msg)
                    {
                        int8_t err = 0xFF;
                        if(overflow)
                        {
#if ((defined TEST) && (!defined NDEBUG))
            REQUIRE_THROWS_WITH( fixedpoint32_subtract(u, v, &err),
                                 Catch::Matchers::StartsWith("assert failed") );
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                            (void) fixedpoint32_subtract(u, v, &err);
                            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                        }
                        else
                        {
                            // UUT
                            tFixedpoint32 diff = fixedpoint32_subtract(u, v, &err);
                            REQUIRE_THAT(test_fixedpoint32ToFloat(diff), Catch::Matchers::WithinAbs(expectedDiffFloat, EPSILON_Q16_16));
                            REQUIRE(FIXEDPOINT32_OK == err);
                            REQUIRE(16U == diff.fracLen);
                        }
                    }
                }
            }
        }
    }
}


SCENARIO("Fixed-point subtract: tests for Q8.24 and Q16.16 input data", "[subtract_Q8.24_and_Q16.16]")
{
    char msg[120U] = {0};
    /* generate 10 equally spaced 'u' input data values and 9 'v' values */
    size_t uSteps = 10U;
    double uIncrement = (MAX_Q8_24_VALUE - MIN_Q8_24_VALUE) / (uSteps - 1U);
    size_t vSteps = 9U;
    double vIncrement = 1.0 + (MAX_Q16_16_VALUE - MIN_Q16_16_VALUE) / (uSteps - 1U);

    for(size_t i = 0U; i < uSteps; i++)
    {
        double uFloat = MIN_Q8_24_VALUE + (uIncrement * i);
        tFixedpoint32 u = test_floatToFixedpoint32(uFloat, 24U);
        snprintf(msg, sizeof(msg), "The first input value is %f in Q8.24 fixed-point format", uFloat);
        WHEN(msg)
        {
            for(size_t j = 0U; j < vSteps; j++)
            {
                double vFloat = MIN_Q16_16_VALUE + (vIncrement * j);
                tFixedpoint32 v = test_floatToFixedpoint32(vFloat, 16U);
                snprintf(msg, sizeof(msg), "The second input value is %f in Q16.16 fixed-point format", vFloat);
                AND_WHEN(msg)
                {
                    double expectedDiffFloat = uFloat - vFloat;
                    bool overflow = ((MIN_Q16_16_VALUE > expectedDiffFloat) || (MAX_Q16_16_VALUE < expectedDiffFloat));
                    if(overflow)
                    {
                        /* arithmetic overflow has occurred */
#if ((defined TEST) && (!defined NDEBUG))
                        snprintf(msg, sizeof(msg), "Subtracting input value %f from %f shall cause assert failure because difference is out of range", vFloat, uFloat);
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                        snprintf(msg, sizeof(msg), "Subtracting input value %f from %f shall cause FIXEDPOINT32_OVERFLOW", vFloat, uFloat);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                    }
                    else
                    {
                        snprintf(msg, 100, "The difference of input values %f and %f shall be %f",
                                 uFloat, vFloat, expectedDiffFloat);
                    }
                    THEN(msg)
                    {
                        int8_t err = 0xFF;
                        if(overflow)
                        {
#if ((defined TEST) && (!defined NDEBUG))
            REQUIRE_THROWS_WITH( fixedpoint32_subtract(u, v, &err),
                                 Catch::Matchers::StartsWith("assert failed") );
#else   /* #if ((defined TEST) && (!defined NDEBUG)) */
                            (void) fixedpoint32_subtract(u, v, &err);
                            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */
                        }
                        else
                        {
                            // UUT
                            tFixedpoint32 diff = fixedpoint32_subtract(u, v, &err);
                            REQUIRE_THAT(test_fixedpoint32ToFloat(diff), Catch::Matchers::WithinAbs(expectedDiffFloat, EPSILON_Q16_16));
                            REQUIRE(FIXEDPOINT32_OK == err);
                            REQUIRE(16U == diff.fracLen);
                        }
                    }
                }
            }
        }
    }
}


SCENARIO("Fixed-point subtract: tests for error conditions", "[subtract_errors]")
{

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("u.FracLen is greater than 31")
    {
        const tFixedpoint32 u = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};
        const tFixedpoint32 v = test_floatToFixedpoint32(1.0, 16U);

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( fixedpoint32_subtract(u, v, NULL),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("v.FracLen is greater than 31")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(1.0, 16U);
        const tFixedpoint32 v = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( fixedpoint32_subtract(u, v, NULL),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("u is -32768.0 and v is 1.0 in Q16.16 fixed-point format")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(-32768.0, 16U);
        const tFixedpoint32 v = test_floatToFixedpoint32(1.0, 16U);

        THEN("An assert failure shall occur because the difference -32769.0 is out-of-range")
        {
            REQUIRE_THROWS_WITH( fixedpoint32_subtract(u, v, NULL),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("u.FracLen is greater than 31")
    {
        const tFixedpoint32 u = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};
        const tFixedpoint32 v = test_floatToFixedpoint32(1.0, 16U);

        THEN("fixedpoint_subtract() shall set error code FIXEDPOINT32_FRAC_LEN_ERROR and return 0.0")
        {
            int8_t err = FIXEDPOINT32_OK;
            double expectedDiffFloat = 0.0;

            // UUT
            tFixedpoint32 diff = fixedpoint32_subtract(u, v, &err);
            REQUIRE(FIXEDPOINT32_FRAC_LEN_ERROR == err);
            REQUIRE_THAT(test_fixedpoint32ToFloat(diff), Catch::Matchers::WithinAbs(expectedDiffFloat, EPSILON));
        }
    }

    WHEN("v.FracLen is greater than 31")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(1.0, 16U);
        const tFixedpoint32 v = {.data = {0x00U, 0x00U, 0x01U, 0x00U}, .fracLen = 32U};

        THEN("fixedpoint_subtract() shall set error code FIXEDPOINT32_FRAC_LEN_ERROR and return 0.0")
        {
            int8_t err = FIXEDPOINT32_OK;
            double expectedDiffFloat = 0.0;

            // UUT
            tFixedpoint32 diff = fixedpoint32_subtract(u, v, &err);
            REQUIRE(FIXEDPOINT32_FRAC_LEN_ERROR == err);
            REQUIRE_THAT(test_fixedpoint32ToFloat(diff), Catch::Matchers::WithinAbs(expectedDiffFloat, EPSILON));
        }
    }

    WHEN("u is -32768.0 and v is 1.0 in Q16.16 fixed-point format")
    {
        const tFixedpoint32 u = test_floatToFixedpoint32(-32768.0, 16U);
        const tFixedpoint32 v = test_floatToFixedpoint32(1.0, 16U);

        THEN("The difference -32769.0 is out-of-range and fixedpoint_subtract() shall set error code FIXEDPOINT32_OVERFLOW")
        {
            int8_t err = FIXEDPOINT32_OK;

            // UUT
            (void) fixedpoint32_subtract(u, v, &err);
            REQUIRE(FIXEDPOINT32_OVERFLOW == err);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}
