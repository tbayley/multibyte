/*
 * test_multibyte_shift.c - Unit tests: multi-byte bit-shift operations.
 *
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include "test.h"
#include "multibyte.h"
#include "catch.hpp"


/* ----- LEFT-SHIFT TESTS -------------------------------------------------- */

SCENARIO("Left-shift tests for 32-bit unsigned input data", "[lshiftUint32]")
{
    char msg[100] = {0};

    /* test vector defines input parameters and the expected output */
    const struct {
        uint32_t u;                 // 32-bit input value
        uint8_t shift;              // number of bits to left-shift
        uint32_t expectedOutput;    // expected ouptut value
    } testVector[12] =
    {
       // input     shift  output
        { 0x12345678,   0, 0x12345678 },
        { 0x12345678,   4, 0x23456780 },
        { 0x12345678,   8, 0x34567800 },
        { 0x12345678,  12, 0x45678000 },
        { 0x12345678,  16, 0x56780000 },
        { 0x12345678,  20, 0x67800000 },
        { 0x12345678,  24, 0x78000000 },
        { 0x12345678,  28, 0x80000000 },
        { 0x12345678,  32, 0x00000000 },
        { 0x12345678, 255, 0x00000000 },
        { 0x00000000,  12, 0x00000000 },
        { 0xFFFFFFFF,  20, 0xFFF00000 }
    };

    for(size_t i = 0; i < sizeof(testVector) / sizeof(testVector[0]); i++)
    {
        snprintf(msg, 100, "Test %lu left-shifts the input value 0x%08X is by %u bits", i, testVector[i].u, testVector[i].shift);
        WHEN(msg)
        {
            const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(testVector[i].u);
            const uint8_t shift = testVector[i].shift;
            snprintf(msg, 100, "The result of test %lu shall be 0x%08X", i, testVector[i].expectedOutput);
            THEN(msg)
            {
                const uint8_t expectedW[4] = UINT32_AS_BYTE_ARRAY(testVector[i].expectedOutput);
                uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                // UUT
                int8_t result = multibyte_lshift(w, u, shift, sizeof(u));
                REQUIRE(MULTIBYTE_OK == result);
                REQUIRE(test_byteArraysAreEqual(expectedW, w, sizeof(expectedW)));
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t shift = 8;
        uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( multibyte_lshift(w, u, shift, sizeof(u)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t shift = 8;
        uint8_t* w = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( multibyte_lshift(w, u, shift, sizeof(u)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t shift = 8;
        uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = multibyte_lshift(w, u, shift, sizeof(u));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedW[4] = {0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedW, w, sizeof(expectedW)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t shift = 8;
        uint8_t* w = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = multibyte_lshift(w, u, shift, sizeof(u));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

/* ----- RIGHT-SHIFT TESTS ------------------------------------------------- */

SCENARIO("Right-shift tests for 32-bit unsigned input data", "[rshiftUint32]")
{
    char msg[100] = {0};

    /* test vector defines input parameters and the expected output */
    const struct {
        uint32_t u;                 // 32-bit input value
        uint8_t shift;              // number of bits to left-shift
        uint32_t expectedOutput;    // expected ouptut value
    } testVector[12] =
    {
       // input     shift  output
        { 0x12345678,   0, 0x12345678 },
        { 0x12345678,   4, 0x01234567 },
        { 0x12345678,   8, 0x00123456 },
        { 0x12345678,  12, 0x00012345 },
        { 0x12345678,  16, 0x00001234 },
        { 0x12345678,  20, 0x00000123 },
        { 0x12345678,  24, 0x00000012 },
        { 0x12345678,  28, 0x00000001 },
        { 0x12345678,  32, 0x00000000 },
        { 0x12345678, 255, 0x00000000 },
        { 0x00000000,  12, 0x00000000 },
        { 0xFFFFFFFF,  20, 0x00000FFF }
    };

    for(size_t i = 0; i < sizeof(testVector) / sizeof(testVector[0]); i++)
    {
        snprintf(msg, 100, "Test %lu right-shifts the input value 0x%08X is by %u bits", i, testVector[i].u, testVector[i].shift);
        WHEN(msg)
        {
            const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(testVector[i].u);
            const uint8_t shift = testVector[i].shift;
            snprintf(msg, 100, "The result of test %lu shall be 0x%08X", i, testVector[i].expectedOutput);
            THEN(msg)
            {
                const uint8_t expectedW[4] = UINT32_AS_BYTE_ARRAY(testVector[i].expectedOutput);
                uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                // UUT
                int8_t result = MULTIBYTE_RSHIFT_UNSIGNED(w, u, shift, sizeof(u));
                REQUIRE(MULTIBYTE_OK == result);
                REQUIRE(test_byteArraysAreEqual(expectedW, w, sizeof(expectedW)));
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t shift = 8;
        uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_RSHIFT_UNSIGNED(w, u, shift, sizeof(u)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t shift = 8;
        uint8_t* w = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_RSHIFT_UNSIGNED(w, u, shift, sizeof(u)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t shift = 8;
        uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_RSHIFT_UNSIGNED(w, u, shift, sizeof(u));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedW[4] = {0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedW, w, sizeof(expectedW)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = UINT32_AS_BYTE_ARRAY(1234U);
        const uint8_t shift = 8;
        uint8_t* w = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_RSHIFT_UNSIGNED(w, u, shift, sizeof(u));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}

SCENARIO("Right-shift tests for 32-bit signed input data", "[rshiftInt32]")
{
    char msg[100] = {0};

    /* test vector defines input parameters and the expected output */
    const struct {
        int32_t u;                  // 32-bit input value
        uint8_t shift;              // number of bits to left-shift
        int32_t expectedOutput;     // expected ouptut value
    } testVector[22] =
    {
        // positive input values
        { 0x12345678,   0, 0x12345678 },
        { 0x12345678,   4, 0x01234567 },
        { 0x12345678,   8, 0x00123456 },
        { 0x12345678,  12, 0x00012345 },
        { 0x12345678,  16, 0x00001234 },
        { 0x12345678,  20, 0x00000123 },
        { 0x12345678,  24, 0x00000012 },
        { 0x12345678,  28, 0x00000001 },
        { 0x12345678,  32, 0x00000000 },
        { 0x12345678, 255, 0x00000000 },
        // negative input values
        { (int32_t) 0x87654321,   0, (int32_t) 0x87654321 },
        { (int32_t) 0x87654321,   4, (int32_t) 0xF8765432 },
        { (int32_t) 0x87654321,   8, (int32_t) 0xFF876543 },
        { (int32_t) 0x87654321,  12, (int32_t) 0xFFF87654 },
        { (int32_t) 0x87654321,  16, (int32_t) 0xFFFF8765 },
        { (int32_t) 0x87654321,  20, (int32_t) 0xFFFFF876 },
        { (int32_t) 0x87654321,  24, (int32_t) 0xFFFFFF87 },
        { (int32_t) 0x87654321,  28, (int32_t) 0xFFFFFFF8 },
        { (int32_t) 0x87654321,  32, (int32_t) 0xFFFFFFFF },
        { (int32_t) 0x87654321, 255, (int32_t) 0xFFFFFFFF },
        // maximum and minimum values
        { (int32_t) 0x00000000,  12, (int32_t) 0x00000000 },
        { (int32_t) 0xFFFFFFFF,  20, (int32_t) 0xFFFFFFFF }
    };

    for(size_t i = 0; i < sizeof(testVector) / sizeof(testVector[0]); i++)
    {
        snprintf(msg, 100, "Test %lu right-shifts the input value 0x%08X is by %u bits", i, testVector[i].u, testVector[i].shift);
        WHEN(msg)
        {
            const uint8_t u[4] = INT32_AS_BYTE_ARRAY(testVector[i].u);
            const uint8_t shift = testVector[i].shift;
            snprintf(msg, 100, "The result of test %lu shall be 0x%08X", i, testVector[i].expectedOutput);
            THEN(msg)
            {
                const uint8_t expectedW[4] = INT32_AS_BYTE_ARRAY(testVector[i].expectedOutput);
                uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage
                // UUT
                int8_t result = MULTIBYTE_RSHIFT_SIGNED(w, u, shift, sizeof(u));
                REQUIRE(MULTIBYTE_OK == result);
                REQUIRE(test_byteArraysAreEqual(expectedW, w, sizeof(expectedW)));
            }
        }
    }

#if ((defined TEST) && (!defined NDEBUG))

    /* Run-time assert() is enabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t shift = 8;
        uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_RSHIFT_SIGNED(w, u, shift, sizeof(u)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(-1234);
        const uint8_t shift = 8;
        uint8_t* w = NULL;

        THEN("An assert failure shall occur")
        {
            REQUIRE_THROWS_WITH( MULTIBYTE_RSHIFT_SIGNED(w, u, shift, sizeof(u)),
                                 Catch::Matchers::StartsWith("assert failed") );
        }
    }

#else   /* #if ((defined TEST) && (!defined NDEBUG)) */

    /* Run-time assert() is disabled */

    WHEN("Input parameter pointer u is NULL")
    {
        const uint8_t* u = NULL;
        const uint8_t shift = 8;
        uint8_t w[4] = {0xAA, 0xAA, 0xAA, 0xAA};  // initialise the output array with garbage

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_RSHIFT_SIGNED(w, u, shift, sizeof(u));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
            /* the output byte array shall not be changed */
            uint8_t expectedW[4] = {0xAA, 0xAA, 0xAA, 0xAA};
            REQUIRE(test_byteArraysAreEqual(expectedW, w, sizeof(expectedW)));
        }
    }

    WHEN("Output parameter pointer w is NULL")
    {
        const uint8_t u[4] = INT32_AS_BYTE_ARRAY(-1234);
        const uint8_t shift = 8;
        uint8_t* w = NULL;

        THEN("The result shall be error code MULTIBYTE_NULL_PARAMETER")
        {
            // UUT
            int8_t result = MULTIBYTE_RSHIFT_SIGNED(w, u, shift, sizeof(u));
            REQUIRE(MULTIBYTE_NULL_PARAMETER == result);
        }
    }

#endif  /* #if ((defined TEST) && (!defined NDEBUG)) */

}
