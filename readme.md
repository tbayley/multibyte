# MULTIBYTE INTEGER AND FIXED-POINT ARITHMETIC #

This project implements multibyte integer and fixed-point arithmetic functions.
These functions enable high-precision DSP algorithms to be implemented on low
performance embedded processors, such as 8-bit microcontrollers.

The _multibyte_ project's Doxygen documentation manual [refman.pdf](./doxygen/latex/refman.pdf)
includes detailed descriptions of the software.


## Multibyte arithmetic ##

Multibyte integer arithmetic enables low perfomance embedded processors, such as
8-bit microcontrollers, to use integer word lengths that are not directly
supported by their compilers. As well as supporting common word-lengths such as
32-bit or 64-bit, _multibyte_ enables arbitrarily long words such as 256-bit or
1024-bit to be used.

Multibyte arithmetic can also be used on higher-performance processors to
perform integer and fixed-point arithmetic using larger word lengths than are
natively supported by the processor and its C compiler. However, it would be
computationally more efficient to write similar multi-word arithmetic functions
that use the processor's native word length, instead of directly using these
multibyte functions.

The algorithms for multibyte addition, subtraction and multiplication are
mostly derived from source code in the book:

> Warren Jr., Henry S. (2013). Hacker's Delight (2 ed.). Addison Wesley
> Pearson Education, Inc. ISBN 978-0-321-84268-8.

## Fixed-point arithmetic ##

Fixed-point arithmetic is essentially the same as integer arithmetic, except
that the value assigned to the least-significant bit (LSB) is some value other
than 1. Normally the LSB value is a fractional power of 2 (e.g. 1/2, 1/4, 1/256,
etc) to enable fractional values to be represented with the required precision.

A 24-bit signed integer with LSB value equal to 1/256 is referred to as _Q16.8_
fixed-point format. _Q_ denotes signed fixed-point format, _16_ denotes the
number of integer-bits (i.e. bit positions that are assigned a value >= 1),
including the sign bit. _8_ denotes the number of fraction-bits
(i.e bit positions that are assigned a value < 1) and is also the LSB's
fractional power of 2: i.e. 1/(2^8).

Similarly, a 16-bit unsigned integer with LSB value equal to 1/16 is referred to
as _UQ12.4_ fixed-point format. _UQ_ denotes unsigned fixed-point format, _12_
denotes the number of integer bits and _4_ denotes the number of fraction bits.

The files _fixedpoint32.h_ and _fixedpoint32.c_ implement fixed-point addition,
subtraction and multiplication functions for 32-bit signed fixed point formats.
The number of fraction bits _fracLen_ is specified at run-time.

The arbitrary scaling of fixed-point bit values requires some rules to be
followed to perform fixed point arithmetic.

When adding or subtracting fixed-point values, the two operands must be
bit-aligned by left or right-shifted them to align their binary point positions.
This ensures that each bit position has the same value for both operands. The
number of integer-bits in the result is one greater than the number of
integer-bits in the bit-aligned input operands, and the number of fraction-bits
in the result remains the same as the number of fraction-bits in the bit-aligned
input operands.

When multiplying fixed-point values, the two operands do not need to be
bit-aligned. For full-precision multiplication, the number of fraction-bits in
the result is equal to the sum of the numbers of fraction-bits in the two
operands, and the number of integer-bits in the result is equal to the sum of
the numbers of integer-bits in the two operands. Sometimes reduced-precision
multiplication is performed, to yield a result that has the same number of
integer and fraction bits as the input operands. Reduced-precision
multiplication can result in overflow or loss of precision.


## Unit tests ##

The unit tests are intended to be built with the GCC compiler and use the
Python-based [SCons](https://scons.org/) build system. Unit tests can be run
either on a Linux computer, or on a Windows computer using
_Windows Subsystem for Linux (WSL)_. For details of how to build and run the
unit tests, see [test/readme.md](test/readme.md).

The primary function of the unit tests is to verify that the multibyte integer
and fixed-point arithmetic functions produce the correct results. However, they
also provide a useful reference for how to use the functions.


## Doxygen documentation ##

To generate [Doxygen](https://www.doxygen.nl) documentation for this project,
first install Doxygen on your computer as described in the
[Doxygen manual](https://www.doxygen.nl/manual/install.html). Then, open a
terminal session in the _multibyte_ project's root directory and enter the
following command:

```bash
doxygen Doxyfile
```

HTML documentation is built in the subdirectory _doxygen/html_. Open the index
page _doxygen/html/index.html_ with a web-browser to view the documentation. 

To generate PDF documentation, enter the following commands:

```bash
cd doxygen/latex
make pdf
```

These commands generate a PDF reference manual [refman.pdf](./doxygen/latex/refman.pdf)
for the _multibyte_ project in subdirectory _doxygen/latex_. For convenience,
the reference manual has been committed to the _multibyte_ git repository, so
that users can view it without having to clone the project and build the
documentation themselves.
