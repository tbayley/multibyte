/*
 * multibyte.c - Multi-byte arithmetic for arbitrary word-length integers.
 * 
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "multibyte.h"
#include "multibyte_assert.h"
#include <stddef.h>


/* MACROS ********************************************************************/

/*! Return the maximum of two uint8_t input values.
 *
 *  @param  u   First uint8_t input value.
 *  @param  v   Second uint8_t input value.
 *
 *  @return     The maximum of u and v.
 */
#define MAX_UINT8( u, v )   \
   ({ uint8_t _u = (u);     \
      uint8_t _v = (v);     \
     _u > _v ? _u : _v; })


/*! Return true if the signed arbitrary word-length integer value is negative.
 *
 *  @param  u       Signed arbitrary word-length integer value, as uint8_t array in
 *                    little-endian byte order.
 *  @param  uLen    Input word-length, in bytes.
 *
 *  @return         True (0x80) if negative, false (0x00) if positive.
 */
#define IS_NEGATIVE(u, uLen)  (u[uLen - 1] & 0x80)  // negative if MSB of most significant byte is set


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

int8_t multibyte_multiply( uint8_t w[], const uint8_t u[], const uint8_t v[],
                         const uint8_t uLen, const uint8_t vLen, const bool isSigned )
{
    int8_t result = MULTIBYTE_NULL_PARAMETER;

    /* check for NULL pointers */
    MULTIBYTE_ASSERT(NULL != w);
    MULTIBYTE_ASSERT(NULL != u);
    MULTIBYTE_ASSERT(NULL != v);
    if((NULL != w) && (NULL != u) && (NULL != v))
    {
        /* Check that in-place multiplication is not being attempted */
        MULTIBYTE_ASSERT((w != u) && (w != v));
        if((w == u) || (w == v))
        {
            result = MULTIBYTE_OPERATION_NOT_SUPPORTED;
        }
        else
        {
            result = MULTIBYTE_OK;
            for(uint8_t i = 0; i < uLen; i++)
            {
                w[i] = 0;
            }

            for(uint8_t j = 0; j < vLen; j++)
            {
                uint16_t k = 0;
                for(uint8_t i = 0; i < uLen; i ++)
                {
                    uint16_t t = u[i] * v[j] + w[i + j] + k;
                    w[i + j] = (uint8_t) (t & 0xFF);  // least significant byte
                    k = t >> 8;
                }
                w[j + uLen] = (uint8_t) (k & 0xFF);  // least significant byte
            }

            /* w[] contains the unsigned product. If data are signed, perform correction */
            if(isSigned)
            {
                if((int8_t) u[uLen - 1] < 0)
                {
                    uint16_t b = 0;  // initialise borrow
                    for(uint8_t j = 0; j < vLen; j++)
                    {
                        uint16_t t = w[j + uLen] - v[j] - b;
                        w[j + uLen] = (uint8_t) (t & 0xFF);  // least significant byte
                        b = t >> 15;
                    }
                }
                if((int8_t) v[vLen - 1] < 0)
                {
                    uint16_t b = 0;  // initialise borrow
                    for(uint8_t i = 0; i < uLen; i++)
                    {
                        uint16_t t = w[i + vLen] - u[i] - b;
                        w[i + vLen] = (uint8_t) (t & 0xFF);  // least significant byte
                        b = t >> 15;
                    }
                }
            }
        }
    }
    return result;
}


int8_t multibyte_add( uint8_t w[], const uint8_t u[], const uint8_t v[],
                      const uint8_t uLen, const uint8_t vLen,
                      const bool isSigned, const bool growLen )
{
    int8_t result = MULTIBYTE_NULL_PARAMETER;

    /* check for NULL pointers */
    MULTIBYTE_ASSERT(NULL != w);
    MULTIBYTE_ASSERT(NULL != u);
    MULTIBYTE_ASSERT(NULL != v);
    if((NULL != w) && (NULL != u) && (NULL != v))
    {
        /* calculate the padding bytes to be used for sign extension */
        uint8_t uSignExtensionByte = 0U;
        uint8_t vSignExtensionByte = 0U;
        if(isSigned)
        {
            if(IS_NEGATIVE(u, uLen))
            {
                uSignExtensionByte = 0xFFU;
            }
            if(IS_NEGATIVE(v, vLen))
            {
                vSignExtensionByte = 0xFFU;
            }
        }

        /* Save most significant bits of the sign-extended input values for use in
        * overflow-detection. Input values may be overwritten by in-place addition.
        */
        uint8_t maxLen = MAX_UINT8(uLen, vLen);
        uint8_t uMsb = (maxLen == uLen) ? (u[uLen - 1] & 0x80) : (uSignExtensionByte & 0x80);
        uint8_t vMsb = (maxLen == vLen) ? (v[vLen - 1] & 0x80) : (vSignExtensionByte & 0x80);

        /* Add each pair of bytes in sequence from LSByte to MSByte of the output
        * word-length. The shorter input word is MSB-padded with the calculated
        * sign extension byte value.
        */
        uint8_t carry = 0;
        if(growLen)
        {
            maxLen += 1;
        }
        for(uint8_t i = 0; i < maxLen; i++)
        {
            uint8_t uByte = (i < uLen) ? u[i] : uSignExtensionByte;
            uint8_t vByte = (i < vLen) ? v[i] : vSignExtensionByte;
            w[i] = uByte + vByte + carry;
            carry = ( (uByte & vByte) | ((uByte | vByte) & ~w[i]) ) >> 7;
        }

        /* For signed integers, overflow has occurred if the sign (MSB) of the
        * two inputs are same, and the sign of the sum is opposite to the sign
        * of the inputs.
        * 
        * For unsigned integers, overflow has occurred if carry == 1.
        */
        if((isSigned && (uMsb == vMsb) && (uMsb != (w[maxLen - 1] & 0x80))) ||
        (!isSigned && carry))
        {
            result = MULTIBYTE_OVERFLOW;
        }
        else
        {
            result = MULTIBYTE_OK;
        }

    }
    return result;
}


int8_t multibyte_subtract( uint8_t w[], const uint8_t u[], const uint8_t v[],
                           const uint8_t uLen, const uint8_t vLen,
                           const bool isSigned, const bool growLen )
{
    int8_t result = MULTIBYTE_NULL_PARAMETER;

    /* check for NULL pointers */
    MULTIBYTE_ASSERT(NULL != w);
    MULTIBYTE_ASSERT(NULL != u);
    MULTIBYTE_ASSERT(NULL != v);
    if((NULL != w) && (NULL != u) && (NULL != v))
    {
        /* calculate the padding bytes to be used for sign extension */
        uint8_t uSignExtensionByte = 0U;
        uint8_t vSignExtensionByte = 0U;
        if(isSigned)
        {
            if(IS_NEGATIVE(u, uLen))
            {
                uSignExtensionByte = 0xFFU;
            }
            if(IS_NEGATIVE(v, vLen))
            {
                vSignExtensionByte = 0xFFU;
            }
        }

        /* Save most significant bits of the sign-extended input values for use in
        * overflow-detection. Input values may be overwritten by in-place addition.
        */
        uint8_t maxLen = MAX_UINT8(uLen, vLen);
        uint8_t uMsb = (maxLen == uLen) ? (u[uLen - 1] & 0x80) : (uSignExtensionByte & 0x80);
        uint8_t vMsb = (maxLen == vLen) ? (v[vLen - 1] & 0x80) : (vSignExtensionByte & 0x80);

        /* Subtract each pair of bytes in sequence from LSByte to MSByte of the
        * output word-length. The shorter input word is MSB-padded with the calculated
        * sign extension byte value.
        */
        uint8_t borrow = 0;
        if(growLen)
        {
            maxLen += 1;
        }
        for(uint8_t i = 0; i < maxLen; i++)
        {
            uint8_t uByte = (i < uLen) ? u[i] : uSignExtensionByte;
            uint8_t vByte = (i < vLen) ? v[i] : vSignExtensionByte;
            w[i] = uByte - vByte - borrow;
            borrow = ( (~uByte & vByte) | ((~(uByte ^ vByte)) & w[i]) ) >> 7;
        }

        /* For signed integers, overflow has occurred if the sign (MSB) of the
        * two inputs are different, and the sign of the sum is opposite to the
        * sign of the first input (u).
        * 
        * For unsigned integers, overflow has occurred if borrow == 1.
        */
        if((isSigned && (uMsb != vMsb) && (uMsb != (w[maxLen - 1] & 0x80))) ||
        (!isSigned && borrow))
        {
            result = MULTIBYTE_OVERFLOW;
        }
        else
        {
            result = MULTIBYTE_OK;
        }
    }
    return result;
}


int8_t multibyte_lshift( uint8_t w[], const uint8_t u[], const uint8_t shift, 
                         const uint8_t uLen )
{
    int8_t result = MULTIBYTE_NULL_PARAMETER;

    /* check for NULL pointers */
    MULTIBYTE_ASSERT(NULL != w);
    MULTIBYTE_ASSERT(NULL != u);
    if((NULL != w) && (NULL != u))
    {
        uint8_t byteShift = shift / 8;  // number of whole bytes to shift
        uint8_t bitShift = shift % 8;   // number of additional bits to shift

        /* left-shift each byte of the input word, starting at the most-significant byte */
        for(uint8_t i = (uLen - 1); i != 0xFF; i--)
        {
            uint8_t upperByte = (byteShift > i) ? 0U : u[i - byteShift];
            uint8_t lowerByte = (byteShift > (i - 1)) ? 0U : u[i - 1 - byteShift];
            w[i] = (upperByte << bitShift) | lowerByte >> (8 - bitShift);
        }
        result = MULTIBYTE_OK;
    }
    return result;
}


int8_t multibyte_rshift( uint8_t w[], const uint8_t u[], const uint8_t shift, 
                         const uint8_t uLen, const bool isSigned )
{
    int8_t result = MULTIBYTE_NULL_PARAMETER;

    /* check for NULL pointers */
    MULTIBYTE_ASSERT(NULL != w);
    MULTIBYTE_ASSERT(NULL != u);
    if((NULL != w) && (NULL != u))
    {
        /* calculate the padding bytes to be used for sign extension */
        uint8_t uSignExtensionByte = 0U;
        if(isSigned)
        {
            if(IS_NEGATIVE(u, uLen))
            {
                uSignExtensionByte = 0xFFU;
            }
        }

        uint8_t byteShift = shift / 8;  // number of whole bytes to shift
        uint8_t bitShift = shift % 8;   // number of additional bits to shift

        /* right-shift each byte of the input word, starting at the least-significant byte */
        for(uint8_t i = 0; i < uLen; i++)
        {
            uint8_t upperByte = ((i + byteShift + 1) >= uLen) ? uSignExtensionByte : u[i + byteShift + 1];
            uint8_t lowerByte = ((i + byteShift) >= uLen) ? uSignExtensionByte : u[i + byteShift];
            w[i] = (upperByte << (8 - bitShift)) | lowerByte >> bitShift;
        }
        result = MULTIBYTE_OK;
    }
    return result;
}
