/*
 * multibyte.h - Multi-byte arithmetic for arbitrary word-length integers.
 * 
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*! @file multibyte.h
 *
 * C header file for multibyte module.
 * 
 * The [multibyte](@ref multibyte) module implements functions for arbitrary
 * word-length multi-byte integer arithmetic.
 */

#ifndef MULTIBYTE_H
#define MULTIBYTE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>


/*! @defgroup multibyte multibyte
 *
 * The _multibyte_ module implements functions for arbitrary word-length
 * multibyte integer arithmetic.
 *
 * Multibyte integer arithmetic enables low performance embedded processors,
 * such as 8-bit microcontrollers, to use integer word lengths that are not
 * directly supported by their compilers. As well as supporting common
 * word-lengths such as 32-bit or 64-bit, the _multibyte_ module enables
 * arbitrarily long words such as 256-bit or 1024-bit to be used.
 *
 * Multibyte arithmetic can also be used on higher-performance processors to
 * perform integer and fixed-point arithmetic using larger word lengths than are
 * natively supported by the processor and its C compiler. However, it would be
 * computationally more efficient to write similar multi-word arithmetic
 * functions that use the processor's native word length, instead of directly
 * using these multibyte functions.
 *
 * The algorithms for multibyte addition, subtraction and multiplication are
 * mostly derived from source code in the book:
 * 
 * > Warren Jr., Henry S. (2013). Hacker's Delight (2 ed.). Addison Wesley
 * > Pearson Education, Inc. ISBN 978-0-321-84268-8.
 */

/*! @addtogroup multibyte
 *  @{
 */


/* MACROS ********************************************************************/

/* Function return codes */
#define MULTIBYTE_OK                        0   //!< Success.
#define MULTIBYTE_NULL_PARAMETER            -1  //!< Error: NULL function parameter.
#define MULTIBYTE_OVERFLOW                  -2  //!< Error: Arithmetic overflow.
#define MULTIBYTE_OPERATION_NOT_SUPPORTED   -3  //!< Error: Operation is not supported.


/*!
 * Arbitrary word-length unsigned integer multiplication.
 *
 * This macro multiplies two arbitrary-length unsigned integers that are input
 * as uint8_t byte arrays in little-endian byte order. The word-length of the
 * output variable 'w' is the sum of the word lengths of the input variables: 
 * (uLen + vLen).
 * 
 * Multiplication cannot be performed in-place. Therefore, the output variable
 * 'w' must be a different array from either of the input arrays 'u' and 'v'.
 *
 * This multiplication algorithm is based on the process described in chapter 8
 * of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the multiplication, little-endian byte order
 * @param[in]   u         First input value to be multiplied, little-endian byte order.
 * @param[in]   v         Second input value to be multiplied, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 */
#define MULTIBYTE_MULTIPLY_UNSIGNED( w, u, v, uLen, vLen ) \
            ( multibyte_multiply( w, u, v, uLen, vLen, false ) )


/*!
 * Arbitrary word-length signed integer multiplication.
 *
 * This macro multiplies two arbitrary-length unsigned integers that are input
 * as uint8_t byte arrays in little-endian byte order. The word-length of the
 * output variable 'w' is the sum of the word lengths of the input variables: 
 * (uLen + vLen).
 * 
 * Multiplication cannot be performed in-place. Therefore, the output variable
 * 'w' must be a different array from either of the input arrays 'u' and 'v'.
 *
 * This multiplication algorithm is based on the process described in chapter 8
 * of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the multiplication, little-endian byte order
 * @param[in]   u         First input value to be multiplied, little-endian byte order.
 * @param[in]   v         Second input value to be multiplied, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 */
#define MULTIBYTE_MULTIPLY_SIGNED( w, u, v, uLen, vLen ) \
            ( multibyte_multiply( w, u, v, uLen, vLen, true ) )


/*!
 * Arbitrary word-length unsigned integer addition.
 *
 * This function adds two arbitrary-length unsigned integers that are input as
 * uint8_t byte arrays in little-endian byte order.
 *
 * The 'growLen' parameter sets the output word length, and therefore determines
 * whether arithmetic overflow can occur. If 'growLen' is false, the multibyte
 * addition operation behaves much like C standard library integer addition: The
 * output word length is equal to to the longest of the input word lengths, and
 * arithmetic overflow can occur. If overflow does occur, the function returns
 * the error code MULTIBYTE_OVERFLOW. If 'growLen' is false, the multibyte
 * addition operation uses an output word length one byte longer than the
 * longest input word length, so that arithmetic overflow cannot occur.
 * 
 * Addition can be performed in-place. Therefore, the output variable 'w' may
 * either be one of the input variables 'u' and 'v', or a different array.
 *
 * This addition algorithm is based on the double-length add/subtract process
 * described in section 2-16 of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the addition, little-endian byte order.
 * @param[in]   u         First input value to be added, little-endian byte order.
 * @param[in]   v         Second input value to be added, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       growLen   True: Word length grows. No overflow can occur.
 *                          False: Output and input word lengths equal. Overflow can occur.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
#define MULTIBYTE_ADD_UNSIGNED( w, u, v, uLen, vLen, growLen ) \
            ( multibyte_add( w, u, v, uLen, vLen, false, growLen ) )


/*!
 * Arbitrary word-length signed integer addition.
 *
 * This function adds two arbitrary-length signed integers that are input as
 * uint8_t byte arrays in little-endian byte order.
 *
 * The 'growLen' parameter sets the output word length, and therefore determines
 * whether arithmetic overflow can occur. If 'growLen' is false, the multibyte
 * addition operation behaves much like C standard library integer addition: The
 * output word length is equal to to the longest of the input word lengths, and
 * arithmetic overflow can occur. If overflow does occur, the function returns
 * the error code MULTIBYTE_OVERFLOW. If 'growLen' is false, the multibyte
 * addition operation uses an output word length one byte longer than the
 * longest input word length, so that arithmetic overflow cannot occur.
 * 
 * Addition can be performed in-place. Therefore, the output variable 'w' may
 * either be one of the input variables 'u' and 'v', or a different array.
 *
 * This addition algorithm is based on the double-length add/subtract process
 * described in section 2-16 of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the addition, little-endian byte order.
 * @param[in]   u         First input value to be added, little-endian byte order.
 * @param[in]   v         Second input value to be added, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       growLen   True: Word length grows. No overflow can occur.
 *                          False: Output and input word lengths equal. Overflow can occur.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
#define MULTIBYTE_ADD_SIGNED( w, u, v, uLen, vLen, growLen ) \
            ( multibyte_add( w, u, v, uLen, vLen, true, growLen ) )


/*!
 * Arbitrary word-length unsigned integer subtraction.
 *
 * This function subtracts two arbitrary-length unsigned integers that are input
 * as uint8_t byte arrays in little-endian byte order.
 * 
 * The 'growLen' parameter sets the output word length, and therefore determines
 * whether arithmetic overflow can occur. If 'growLen' is false, the multibyte
 * subtraction operation behaves much like C standard library integer
 * subtraction: The output word length is equal to to the longest of the input
 * word lengths, and arithmetic overflow can occur. If overflow does occur, the
 * function returns the error code MULTIBYTE_OVERFLOW. If 'growLen' is false,
 * the multibyte subtraction operation uses an output word length one byte
 * longer than the longest input word length, so that arithmetic overflow cannot
 * occur.
 * 
 * Subtraction can be performed in-place. Therefore, the output variable 'w' may
 * either be one of the input variables 'u' and 'v', or a different array.
 *
 * This subtraction algorithm is based on the double-length add/subtract process
 * described in section 2-16 of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the subtraction, little-endian byte order.
 * @param[in]   u         First input value, little-endian byte order.
 * @param[in]   v         Second input value that is to be subtracted from the
 *                          first input value, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       growLen   True: Word length grows. No overflow can occur.
 *                          False: Output and input word lengths equal. Overflow can occur.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
#define MULTIBYTE_SUBTRACT_UNSIGNED( w, u, v, uLen, vLen, growLen ) \
            ( multibyte_subtract( w, u, v, uLen, vLen, false, growLen ) )


/*!
 * Arbitrary word-length signed integer subtraction.
 *
 * This function subtracts two arbitrary-length signed integers that are input
 * as uint8_t byte arrays in little-endian byte order.
 * 
 * The 'growLen' parameter sets the output word length, and therefore determines
 * whether arithmetic overflow can occur. If 'growLen' is false, the multibyte
 * subtraction operation behaves much like C standard library integer
 * subtraction: The output word length is equal to to the longest of the input
 * word lengths, and arithmetic overflow can occur. If overflow does occur, the
 * function returns the error code MULTIBYTE_OVERFLOW. If 'growLen' is false,
 * the multibyte subtraction operation uses an output word length one byte
 * longer than the longest input word length, so that arithmetic overflow cannot
 * occur.
 * 
 * Subtraction can be performed in-place. Therefore, the output variable 'w' may
 * either be one of the input variables 'u' and 'v', or a different array.
 *
 * This subtraction algorithm is based on the double-length add/subtract process
 * described in section 2-16 of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the subtraction, little-endian byte order.
 * @param[in]   u         First input value, little-endian byte order.
 * @param[in]   v         Second input value that is to be subtracted from the
 *                          first input value, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       growLen   True: Word length grows. No overflow can occur.
 *                          False: Output and input word lengths equal. Overflow can occur.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
#define MULTIBYTE_SUBTRACT_SIGNED( w, u, v, uLen, vLen, growLen ) \
            ( multibyte_subtract( w, u, v, uLen, vLen, true, growLen ) )


/*!
 * Arbitrary word-length unsigned integer right-shift operation.
 *
 * This function right-shifts an arbitrary-length unsigned integer that is input
 * as a uint8_t byte array in little-endian byte order. The word-length of the
 * output variable 'w' is equal to the input word length 'uLen'.
 * 
 * The 'shift' most-significant bits of output word 'w' are filled with bit
 * value 0.
 *
 * Right-shifting can be performed in-place. Therefore, the output variable 'w'
 * may either be the same as input variable 'u', or a different array.
 *
 * @param[out]  w         Result of the right-shift operation, little-endian byte order.
 * @param[in]   u         Input value to be right-shifted, little-endian byte order.
 * @param       shift     Number of bits to be right-shifted.
 * @param       uLen      Word-length of the input value, in bytes.
 */
#define MULTIBYTE_RSHIFT_UNSIGNED( w, u, shift, uLen ) \
            ( multibyte_rshift( w, u, shift, uLen, false ) )


/*!
 * Arbitrary word-length signed integer right-shift operation.
 *
 * This function right-shifts an arbitrary-length signed integer that is input
 * as a uint8_t byte array in little-endian byte order. The word-length of the
 * output variable 'w' is equal to the input word length 'uLen'.
 * 
 * The 'shift' most-significant bits of output word 'w' are filled with the sign
 * bit of the input word (1 if negative, 0 if positive).
 *
 * Right-shifting can be performed in-place. Therefore, the output variable 'w'
 * may either be the same as input variable 'u', or a different array.
 *
 * @param[out]  w         Result of the right-shift operation, little-endian byte order.
 * @param[in]   u         Input value to be right-shifted, little-endian byte order.
 * @param       shift     Number of bits to be right-shifted.
 * @param       uLen      Word-length of the input value, in bytes.
 */
#define MULTIBYTE_RSHIFT_SIGNED( w, u, shift, uLen ) \
            ( multibyte_rshift( w, u, shift, uLen, true ) )


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 * Arbitrary word-length integer multiplication.
 *
 * This function multiplies two arbitrary-length signed or unsigned integers
 * that are input as uint8_t byte arrays in little-endian byte order. The
 * word-length of the output variable 'w' is the sum of the word lengths of
 * the input variables: (uLen + vLen).
 * 
 * Multiplication cannot be performed in-place. Therefore, the output variable
 * 'w' must be a different array from either of the input arrays 'u' and 'v'.
 *
 * This multiplication algorithm is based on the process described in chapter 8
 * of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the multiplication, little-endian byte order.
 * @param[in]   u         First input value to be multiplied, little-endian byte order.
 * @param[in]   v         Second input value to be multiplied, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       isSigned  True: Input and output values are signed integers.
 *                        False: Input and output values are unsigned integers.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
int8_t multibyte_multiply( uint8_t w[], const uint8_t u[], const uint8_t v[],
                           const uint8_t uLen, const uint8_t vLen, const bool isSigned );


/*!
 * Arbitrary word-length integer addition.
 *
 * This function adds two arbitrary-length signed or unsigned integers that are
 * input as uint8_t byte arrays in little-endian byte order.
 *
 * The 'growLen' parameter sets the output word length, and therefore determines
 * whether arithmetic overflow can occur. If 'growLen' is false, the multibyte
 * addition operation behaves much like C standard library integer addition: The
 * output word length is equal to to the longest of the input word lengths, and
 * arithmetic overflow can occur. If overflow does occur, the function returns
 * the error code MULTIBYTE_OVERFLOW. If 'growLen' is false, the multibyte
 * addition operation uses an output word length one byte longer than the
 * longest input word length, so that arithmetic overflow cannot occur.
 * 
 * Addition can be performed in-place. Therefore, the output variable 'w' may
 * either be one of the input variables 'u' and 'v', or a different byte array.
 *
 * This addition algorithm is based on the double-length add/subtract process
 * described in section 2-16 of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the addition, little-endian byte order.
 * @param[in]   u         First input value to be added, little-endian byte order.
 * @param[in]   v         Second input value to be added, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       isSigned  True: Input and output values are signed integers.
 *                          False: Input and output values are unsigned integers.
 * @param       growLen   True: Word length grows. No overflow can occur.
 *                          False: Output and input word lengths equal. Overflow can occur.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
int8_t multibyte_add( uint8_t w[], const uint8_t u[], const uint8_t v[],
                      const uint8_t uLen, const uint8_t vLen,
                      const bool isSigned, const bool growLen );


/*!
 * Arbitrary word-length integer subtraction.
 *
 * This function subtracts two arbitrary-length signed or unsigned integers that
 * are input as uint8_t byte arrays in little-endian byte order.
 * 
 * The 'growLen' parameter sets the output word length, and therefore determines
 * whether arithmetic overflow can occur. If 'growLen' is false, the multibyte
 * subtraction operation behaves much like C standard library integer
 * subtraction: The output word length is equal to to the longest of the input
 * word lengths, and arithmetic overflow can occur. If overflow does occur, the
 * function returns the error code MULTIBYTE_OVERFLOW. If 'growLen' is false,
 * the multibyte subtraction operation uses an output word length one byte
 * longer than the longest input word length, so that arithmetic overflow cannot
 * occur.
 * 
 * Subtraction can be performed in-place. Therefore, the output variable 'w' may
 * either be one of the input variables 'u' and 'v', or a different byte array.
 *
 * This subtraction algorithm is based on the double-length add/subtract process
 * described in section 2-16 of the book:
 *
 * > Hackers Delight, Second Edition.  
 * > Henry S. Warren, Jr.  
 * > Addison Wesley. 2013.
 *
 * @param[out]  w         Result of the subtraction, little-endian byte order.
 * @param[in]   u         First input value, little-endian byte order.
 * @param[in]   v         Second input value that is to be subtracted from the
 *                          first input value, little-endian byte order.
 * @param       uLen      Word-length of the first input value, in bytes.
 * @param       vLen      Word-length of the second input value, in bytes.
 * @param       isSigned  True: Input and output values are signed integers.
 *                          False: Input and output values are unsigned integers.
 * @param       growLen   True: Word length grows. No overflow can occur.
 *                          False: Output and input word lengths equal. Overflow can occur.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
int8_t multibyte_subtract( uint8_t w[], const uint8_t u[], const uint8_t v[],
                           const uint8_t uLen, const uint8_t vLen,
                           const bool isSigned, const bool growLen );


/*!
 * Arbitrary word-length integer left-shift operation.
 *
 * This function left-shifts an arbitrary-length signed or unsigned integer
 * that is input as a uint8_t byte array in little-endian byte order. The
 * word-length of the output variable 'w' is equal to the input word length
 * 'uLen'.
 * 
 * The 'shift' most-significant bits of input word 'u' are discarded and the
 * 'shift' least-significant bits of output word 'w' are filled with 0.
 * Left-shifting of signed integers should normally be avoided, because it may
 * result in a change to the sign bit.
 *
 * Left-shifting can be performed in-place. Therefore, the output variable 'w'
 * may either be the same as input variable 'u', or a different array.
 *
 * @param[out]  w         Result of the left-shift operation, little-endian byte order.
 * @param[in]   u         Input value to be left-shifted, little-endian byte order.
 * @param       shift     Number of bits to be left-shifted.
 * @param       uLen      Word-length of the input value, in bytes.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
int8_t multibyte_lshift( uint8_t w[], const uint8_t u[], const uint8_t shift, 
                         const uint8_t uLen );


/*!
 * Arbitrary word-length integer right-shift operation.
 *
 * This function right-shifts an arbitrary-length signed or unsigned integer
 * that is input as a uint8_t byte array in little-endian byte order. The
 * word-length of the output variable 'w' is equal to the input word length
 * 'uLen'.
 * 
 * For unsigned input data, the 'shift' most-significant bits of output word 'w'
 * are filled with bit value 0. For signed data, they are filled with the sign
 * bit of the input word (1 if negative, 0 if positive).
 *
 * Right-shifting can be performed in-place. Therefore, the output variable 'w'
 * may either be the same as input variable 'u', or a different array.
 *
 * @param[out]  w         Result of the right-shift operation, little-endian byte order.
 * @param[in]   u         Input value to be right-shifted, little-endian byte order.
 * @param       shift     Number of bits to be right-shifted.
 * @param       uLen      Word-length of the input value, in bytes.
 * @param       isSigned  True: Input and output values are signed integers.
 *                        False: Input and output values are unsigned integers.
 *
 * @return                0 (MULTIBYTE_OK) on success, or error code on failure.
 */
int8_t multibyte_rshift( uint8_t w[], const uint8_t u[], const uint8_t shift, 
                         const uint8_t uLen, const bool isSigned );


/*! @} */   // end of multibyte group
 
#ifdef __cplusplus
}
#endif

#endif  /* MULTIBYTE_H */
