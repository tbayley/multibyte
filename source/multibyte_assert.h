/*
 * multibyte_assert.h - Assert macro for multibyte repository.
 * 
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved. *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*! @file multibyte_assert.h
 *
 * This file defines the _MULTIBYTE_ASSERT()_ macro that is used to check for
 * run-time errors.
 *
 * On embedded systems, assertion failure is usually configured to halt code
 * execution at a software breakpoint. You may want to customise this macro for
 * the specific microcontroller and compiler that is in use. The standard
 * _MULTIBYTE_ASSERT()_ macro halts at a breakpoint when building code for PIC
 * microcontrollers using Microchip MPLAB XC compilers: XC8, XC16 or XC32.
 * However, when using other compilers, its default behaviour is to invoke the
 * C standard library's _assert()_ macro.
 */

#ifndef MULTIBYTE_ASSERT_H
#define MULTIBYTE_ASSERT_H

#if ((defined TEST) && (!defined NDEBUG))
#include <stdexcept>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>


/*! @addtogroup multibyte
 *  @{
 */


/* MACROS ********************************************************************/

/*! Inner nested macro #1 to convert the standard __LINE__ macro to a string. */
#define STRINGIZE(x) STRINGIZE2(x)

/*! Inner nested macro #2 to convert the standard __LINE__ macro to a string. */
#define STRINGIZE2(x) #x

/*! Macro to convert the standard __LINE__ macro to a string.
 *
 * Because of the way macros work, multiple nested macros are required. See
 * https://stackoverflow.com/questions/2670816/how-can-i-use-the-compile-time-constant-line-in-a-string/2671100
 */
#define LINE_STRING STRINGIZE(__LINE__)


/*!
 * Assert macro.
 *
 * The 'expression' parameter evaluates to zero or non-zero. If zero, the
 * assertion fails and the user is notified. On embedded systems, assertion
 * failure should be configured to halt code execution at a software breakpoint.
 * The default behaviour, for other systems, is to print a message to stderr and
 * then call the function abort().
 *
 * Assertion testing may be turned off without removing the code by defining
 * NDEBUG before including <assert.h>. If the macro NDEBUG is defined, assert()
 * is ignored and no code is generated.
 *
 * If TEST is defined, then assert failure causes a C++ runtime_error to be
 * thrown when NDEBUG is not defined. The runtime_error exception can be caught
 * by the Catch2 C++ unit test framework, to check for assert failures in the
 * unit under test.
 *
 * @param  expression     Expression to be evaluated.
 */
#if ((defined TEST) && (!defined NDEBUG))  // unit test: raise runtime_error for Catch2 C++ unit test framework
#define MULTIBYTE_ASSERT( expression )  if( !(expression) ) { throw std::runtime_error("assert failed at " __FILE__ ":" LINE_STRING); }
#elif defined __XC__  // Microchip MPLAB XC compiler: XC8, XC16 or XC32
#define MULTIBYTE_ASSERT( expression )  ( __conditional_software_breakpoint((expression)) )
#else
#define MULTIBYTE_ASSERT( expression )  ( assert((expression)) )
#endif


/*! @} */   // end of multibyte group

#ifdef __cplusplus
}
#endif

#endif  /* MULTIBYTE_ASSERT_H */
