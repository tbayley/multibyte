/*
 * fixedpoint32.h - 32-bit fixed-point arithmetic.
 * 
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*! @file fixedpoint32.h
 *
 * C header file for fixedpoint32 module.
 * 
 * The [fixedpoint32](@ref fixedpoint32) module implements functions for 32-bit
 * fixed-point arithmetic.
 */

#ifndef FIXEDPOINT32_H
#define FIXEDPOINT32_H

#ifdef __cplusplus
extern "C" {
#endif

#include "multibyte.h"


/*! @defgroup fixedpoint32 fixedpoint32
 *
 * The _fixedpoint32_ module implements functions for 32-bit fixed-point
 * arithmetic.
 *
 * Fixed-point arithmetic is essentially the same as integer arithmetic, except
 * that the value assigned to the least-significant bit (LSB) is some value
 * other  than 1. Normally the LSB value is a fractional power of 2 (e.g. 1/2,
 * 1/4, 1/256, etc) to enable fractional values to be represented with the
 * required precision.
 *
 * A 24-bit signed integer with LSB value equal to 1/256 is referred to as
 * _Q16.8_ fixed-point format. _Q_ denotes signed fixed-point format, _16_
 * denotes the number of integer-bits (i.e. bit positions that are assigned a
 * value >= 1), including the sign bit. _8_ denotes the number of fraction-bits
 * (i.e bit positions that are assigned a value < 1) and is also the LSB's
 * fractional power of 2: i.e. 1/(2^8).
 *
 * Similarly, a 16-bit unsigned integer with LSB value equal to 1/16 is referred
 * to as _UQ12.4_ fixed-point format. _UQ_ denotes unsigned fixed-point format,
 * _12_ denotes the number of integer bits and _4_ denotes the number of
 * fraction bits.
 *
 * The files _fixedpoint32.h_ and _fixedpoint32.c_ implement fixed-point
 * addition, subtraction and multiplication functions for 32-bit signed fixed
 * point formats. The number of fraction bits _fracLen_ is specified at
 * run-time.
 * 
 * The arbitrary scaling of fixed-point bit values requires some rules to be
 * followed to perform fixed point arithmetic.
 *
 * When adding or subtracting fixed-point values, the two operands must be
 * bit-aligned by left or right-shifted them to align their binary point
 * positions. This ensures that each bit position has the same value for both
 * operands. The number of integer-bits in the result is one greater than the
 * number of integer-bits in the bit-aligned input operands, and the number of
 * fraction-bits in the result remains the same as the number of fraction-bits
 * in the bit-aligned input operands.
 * 
 * When multiplying fixed-point values, the two operands do not need to be
 * bit-aligned. For full-precision multiplication, the number of fraction-bits
 * in the result is equal to the sum of the numbers of fraction-bits in the two
 * operands, and the number of integer-bits in the result is equal to the sum of
 * the numbers of integer-bits in the two operands. Sometimes reduced-precision
 * multiplication is performed, to yield a result that has the same number of
 * integer and fraction bits as the input operands. Reduced-precision
 * multiplication can result in overflow or loss of precision.
 */

/*! @addtogroup fixedpoint32
 *  @{
 *  @callgraph
 */


/* PUBLIC MACROS *************************************************************/

/* Function return codes */
#define FIXEDPOINT32_OK                 MULTIBYTE_OK                //!< Success.
#define FIXEDPOINT32_NULL_PARAMETER     MULTIBYTE_NULL_PARAMETER    //!< Error: NULL function parameter.
#define FIXEDPOINT32_OVERFLOW           MULTIBYTE_OVERFLOW          //!< Error: Arithmetic overflow.
#define FIXEDPOINT32_FRAC_LEN_ERROR     -4                          //!< Error: FracLen field value greater than 32.


/* PUBLIC TYPE DEFINITIONS ***************************************************/

/*!
 * 32-bit signed fixed-point type.
 */
typedef struct
{
    uint8_t data[4];    //!< 32-bit multibyte integer data.
    uint8_t fracLen;    //!< Number of fraction bits: from 0 to 32 inclusive.
} tFixedpoint32;


/* PUBLIC FUNCTION DECLARATIONS **********************************************/

/*!
 * 32-bit fixed-point multiplication.
 *
 * Multiply two 32-bit signed fixed-point numbers, each of which may have any
 * number of fraction bits. The number of fraction bits in the product is the
 * lower of the 'u' fraction bits and the 'v' fraction bits.
 *
 * @param       u   First input value to be multiplied.
 * @param       v   Second input value to be multiplied.
 * @param[out]  err Error code out pointer, or NULL if no error code is required.
 *
 * @return          Product: u * v.
 */
tFixedpoint32 fixedpoint32_multiply( const tFixedpoint32 u, const tFixedpoint32 v, int8_t* const err );


/*!
 * 32-bit fixed-point addition.
 *
 * Add two 32-bit signed fixed-point numbers, each of which may have any number
 * of fraction bits. The number of fraction bits in the sum is the lower of the
 * 'u' fraction bits and the 'v' fraction bits.
 *
 * @param       u   First input value to be added.
 * @param       v   Second input value to be added.
 * @param[out]  err Error code out pointer, or NULL if no error code is required.
 *
 * @return          Sum: u + v.
 */
tFixedpoint32 fixedpoint32_add( tFixedpoint32 u, tFixedpoint32 v, int8_t* const err );


/*!
 * 32-bit fixed-point subtraction.
 *
 * Subtract two 32-bit signed fixed-point numbers, each of which may have any
 * number of fraction bits. The number of fraction bits in the sum is the lower
 * of the 'u' fraction bits and the 'v' fraction bits.
 *
 * @param       u   First input value.
 * @param       v   Second input value, to be subtracted from the first input value.
 * @param[out]  err Error code out pointer, or NULL if no error code is required.
 *
 * @return          Difference: u - v.
 */
tFixedpoint32 fixedpoint32_subtract( tFixedpoint32 u, tFixedpoint32 v, int8_t* const err );


/*! @} */   // end of fixedpoint32 group

#ifdef __cplusplus
}
#endif

#endif  /* FIXEDPOINT32_H */
