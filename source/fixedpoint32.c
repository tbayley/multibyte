/*
 * fixedpoint32.c - 32-bit fixed-point arithmetic.
 * 
 * Copyright (C) 2021 Antony Bayley.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "fixedpoint32.h"
#include "multibyte_assert.h"
#include <string.h>


/* PRIVATE MACROS ************************************************************/

/*! Return the minimum of two uint8_t input values.
 *
 *  @param  u   First int8_t input value.
 *  @param  v   Second int8_t input value.
 *
 *  @return     The minimum of u and v.
 */
#define MIN_UINT8( u, v )   \
   ({ uint8_t _u = (u);     \
      uint8_t _v = (v);     \
     _u < _v ? _u : _v; })


/* PUBLIC FUNCTION DEFINITIONS ***********************************************/

tFixedpoint32 fixedpoint32_multiply( const tFixedpoint32 u, const tFixedpoint32 v, int8_t* const err )
{
    tFixedpoint32 product = {.data = {0U, 0U, 0U, 0U}, .fracLen = 0U};

    /* check for input value errors */
    MULTIBYTE_ASSERT(u.fracLen < 32U);
    MULTIBYTE_ASSERT(v.fracLen < 32U);
    if((32U <= u.fracLen) || (32U <= v.fracLen))
    {
        if(NULL != err)
        {
            *err = FIXEDPOINT32_FRAC_LEN_ERROR;
        }
    }
    else
    {
        product.fracLen = MIN_UINT8(u.fracLen, v.fracLen);
        uint8_t productData64[8];
        (void) MULTIBYTE_MULTIPLY_SIGNED(productData64, u.data, v.data, 4U, 4U);
        uint8_t rshift = u.fracLen + v.fracLen - product.fracLen;
        if(rshift > 0)
        {
            multibyte_rshift(productData64, productData64, (uint8_t) rshift, 8U, true);
        }
        memcpy(product.data, productData64, 4U);

        /* Overflow detection: The product can be represented as a fixed-point
        * 32-bit value, without overflow, if the 33 most significant bits of
        * the 64-bit  product are either all 0 or all 1.
        */
        MULTIBYTE_ASSERT(((0x00U == (productData64[7] | productData64[6] | productData64[5] | productData64[4])) &&  // bits 32 to 63 are 0
                          (0x00U == (productData64[3] & 0x80))) ||                                                   // bit 31 is 0
                         ((0xFFU == (productData64[7] & productData64[6] & productData64[5] & productData64[4])) &&  // bits 32 to 63 are 1
                          (0x80U == (productData64[3] & 0x80))));                                                    // bit 31 is 1
        if(NULL != err)
        {
            if(((0x00U == (productData64[7] | productData64[6] | productData64[5] | productData64[4])) &&  // bits 32 to 63 are 0
                (0x00U == (productData64[3] & 0x80))) ||                                                   // bit 31 is 0
               ((0xFFU == (productData64[7] & productData64[6] & productData64[5] & productData64[4])) &&  // bits 32 to 63 are 1
                (0x80U == (productData64[3] & 0x80))))                                                     // bit 31 is 1
            {
                *err = FIXEDPOINT32_OK;
            }
            else
            {
                *err = FIXEDPOINT32_OVERFLOW;
            }
        }
    }
    return product;
}


tFixedpoint32 fixedpoint32_add( tFixedpoint32 u, tFixedpoint32 v, int8_t* const err )
{
    tFixedpoint32 sum = {.data = {0U, 0U, 0U, 0U}, .fracLen = 0U};

    /* check for input value errors */
    MULTIBYTE_ASSERT(u.fracLen < 32U);
    MULTIBYTE_ASSERT(v.fracLen < 32U);
    if((32U <= u.fracLen) || (32U <= v.fracLen))
    {
        if(NULL != err)
        {
            *err = FIXEDPOINT32_FRAC_LEN_ERROR;
        }
    }
    else
    {
        sum.fracLen = MIN_UINT8(u.fracLen, v.fracLen);
        if(u.fracLen != v.fracLen)
        {
            /* force u and v to have the same number of fraction bits */
            uint8_t rshift = u.fracLen - sum.fracLen;
            if(0 != rshift)
            {
                u.fracLen -= rshift;
                MULTIBYTE_RSHIFT_SIGNED(u.data, u.data, rshift, 4U);
            }
            else
            {
                rshift = v.fracLen - sum.fracLen;
                v.fracLen -= rshift;
                MULTIBYTE_RSHIFT_SIGNED(v.data, v.data, rshift, 4U);
            }
        }

        int8_t result = MULTIBYTE_ADD_SIGNED(sum.data, u.data, v.data, 4U, 4U, false);
        MULTIBYTE_ASSERT(MULTIBYTE_OK == result);
        if(NULL != err)
        {
            *err = (MULTIBYTE_OK == result) ? FIXEDPOINT32_OK : FIXEDPOINT32_OVERFLOW;
        }
    }
    return sum;
}


tFixedpoint32 fixedpoint32_subtract( tFixedpoint32 u, tFixedpoint32 v, int8_t* const err )
{
    tFixedpoint32 diff = {.data = {0U, 0U, 0U, 0U}, .fracLen = 0U};

    /* check for input value errors */
    MULTIBYTE_ASSERT(u.fracLen < 32U);
    MULTIBYTE_ASSERT(v.fracLen < 32U);
    if((32U <= u.fracLen) || (32U <= v.fracLen))
    {
        if(NULL != err)
        {
            *err = FIXEDPOINT32_FRAC_LEN_ERROR;
        }
    }
    else
    {
        diff.fracLen = MIN_UINT8(u.fracLen, v.fracLen);
        if(u.fracLen != v.fracLen)
        {
            /* force u and v to have the same number of fraction bits */
            uint8_t rshift = u.fracLen - diff.fracLen;
            if(0 != rshift)
            {
                u.fracLen -= rshift;
                MULTIBYTE_RSHIFT_SIGNED(u.data, u.data, rshift, 4U);
            }
            else
            {
                rshift = v.fracLen - diff.fracLen;
                v.fracLen -= rshift;
                MULTIBYTE_RSHIFT_SIGNED(v.data, v.data, rshift, 4U);
            }
        }

        int8_t result = MULTIBYTE_SUBTRACT_SIGNED(diff.data, u.data, v.data, 4U, 4U, false);
        MULTIBYTE_ASSERT(MULTIBYTE_OK == result);
        if(NULL != err)
        {
            *err = (MULTIBYTE_OK == result) ? FIXEDPOINT32_OK : FIXEDPOINT32_OVERFLOW;
        }
    }
    return diff;
}
