# SCONS build script for multibyte project unit tests
#
# Copyright (C) 2021 Antony Bayley.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import platform
import os
import glob
import time

# Check for parallel task execution, which may corrupt test coverage statistics
# SetOption('num_jobs', 1)  # disable parallel task execution
print('running with -j{}'.format(GetOption('num_jobs')))

# Use MinGW compiler on Windows, and the default compiler (g++) on Linux
def get_tools():
    tools = ['default']
    if platform.system() == 'Windows':
        tools = ['mingw']
    return tools

# Include paths
INCLUDE_PATHS = ['test', 'source']

# C preprocessor defines
#   TEST        Define TEST preprocesor symbol (equivalent to #define TEST) for unit tests.
#   NDEBUG      Define NDEBUG preprocessor symbol, to disable assert() for unit tests.
CPP_DEFINES = ['TEST', 'NDEBUG']

# Compile time flags
#   -std=c++11                  Use C++ 11 language standard
#   -Wall -Wextra               Implement all warnings
#   -O0                         Reduce compilation time and make debugging produce the expected results
#   -g                          Generate debugging information
#   --coverage                  Compile code instrumented for coverage analysis
#   -fPIC                       Generate position independent code, if supported on target machine
#   -fsanitize=address          Enable AddressSanitizer memory error detection
#   -fno-omit-frame-pointer     Leave frame pointers. Allows the fast unwinder to function properly
#   -fsanitize=undefined        Enable C/C++ undefined behaviour detection
#   --fno-sanitize-recover      If undefined behaviour is detected, print error report and exit the program.
#                               It is sometimes useful to remove '--fno-sanitize-recover' when debugging.
COMPILE_FLAGS = ['-std=c++11', '-Wall', '-Wextra', '-O0', '-g', '-ftest-coverage', 
                 '-fprofile-arcs', '-fPIC', '-fsanitize=address', '-fno-omit-frame-pointer',
                 '-fsanitize=undefined', '-fno-sanitize-recover']

# Link time flags
#   -g                          Generate debugging information
#   --coverage                  Link code instrumented for coverage analysis
#   -fsanitize=address          Enable AddressSanitizer memory error detection
#   -fsanitize=undefined        Enable C/C++ undefined behaviour detection
LINK_FLAGS = ['-g', '--coverage', '-fsanitize=address', '-fsanitize=undefined']

# Libraries
#   gcov                Test coverage library
#   m                   Math library (C standard library)
LIBRARIES = ['gcov', 'm']

# If Command Line parameter --xmlreport is specified, replace the human-readable
# text unit-test reports by Junit XML reports use by CI server.
AddOption('--xmlreport',
          dest='xmlreport',
          action='store_true',
          default=False)
xmlreport = GetOption('xmlreport')

# Define SCons 'RunUnitTests' Builder to run unit tests and generate coverage statistics
if xmlreport:
    run_cmd = '$SOURCE.abspath -r junit >> $TARGET.abspath'
else:
    run_cmd = '$SOURCE.abspath -r console'
runUnitTestsBuilder = Builder(action = run_cmd)

env = Environment(ENV = {'PATH' : os.environ['PATH'],
                         'HOME' : os.environ['HOME']},
                         tools=get_tools())
env.Append(CPPPATH = INCLUDE_PATHS)
env.Append(CPPDEFINES = CPP_DEFINES)
env.Append(CCFLAGS = COMPILE_FLAGS)
env.Append(LINKFLAGS = LINK_FLAGS)
env.Append(LIBS = LIBRARIES)
env.Append(BUILDERS = {'RunUnitTests' : runUnitTestsBuilder})

# Build .c files with C++ compiler (required for Catch2 unit testing framework)
env['CC'] = env['CXX']


##############################################################################
# BUILD TARGET DEFINITIONS
##############################################################################

# In the file list for each unit test, the unit test source file test_XXX.c
# must be the first list item.

# Unit test file: test_multibyte_add.c
test_multibyte_add = env.Program(source=[
    'test/test_multibyte_add.c',
    'source/multibyte.c',
    'test/test.c',
    'test/runner.c'
])
env.Alias('test_multibyte_add_build', test_multibyte_add)


# Unit test file: test_multibyte_subtract.c
test_multibyte_subtract = env.Program(source=[
    'test/test_multibyte_subtract.c',
    'source/multibyte.c',
    'test/test.c',
    'test/runner.c'
])
env.Alias('test_multibyte_subtract_build', test_multibyte_subtract)

# Unit test file: test_multibyte_multiply.c
test_multibyte_multiply = env.Program(source=[
    'test/test_multibyte_multiply.c',
    'test/test.c',
    'source/multibyte.c',
    'test/runner.c'
])
env.Alias('test_multibyte_multiply_build', test_multibyte_multiply)

# Unit test file: test_multibyte_shift.c
test_multibyte_shift = env.Program(source=[
    'test/test_multibyte_shift.c',
    'source/multibyte.c',
    'test/test.c',
    'test/runner.c'
])
env.Alias('test_multibyte_shift_build', test_multibyte_shift)

# Unit test file: test_fixedpoint32_multiply.c
test_fixedpoint32_multiply = env.Program(source=[
    'test/test_fixedpoint32_multiply.c',
    'test/test.c',
    'source/fixedpoint32.c',
    'source/multibyte.c',
    'test/runner.c'
])
env.Alias('test_fixedpoint32_multiply_build', test_fixedpoint32_multiply)

# Unit test file: test_fixedpoint32_add.c
test_fixedpoint32_add = env.Program(source=[
    'test/test_fixedpoint32_add.c',
    'test/test.c',
    'source/fixedpoint32.c',
    'source/multibyte.c',
    'test/runner.c'
])
env.Alias('test_fixedpoint32_add_build', test_fixedpoint32_add)

# Unit test file: test_fixedpoint32_subtract.c
test_fixedpoint32_subtract = env.Program(source=[
    'test/test_fixedpoint32_subtract.c',
    'test/test.c',
    'source/fixedpoint32.c',
    'source/multibyte.c',
    'test/runner.c'
])
env.Alias('test_fixedpoint32_subtract_build', test_fixedpoint32_subtract)


##############################################################################
# RUN TARGET DEFINITIONS
##############################################################################

# For each unit test executable file, there exists a SCons target that runs the
# file and generates unit test coverage data (.gcda files). Each 'RunUnitTests'
# target has an alias that can be specified at the command line to run the
# tests. The alias is the base name of the executable file, without directory
# path or file extension.
#
# For example the executable "file build/test/test_multibyte_add.exe", which is
# built from source file "test/test_multibyte_add.c", can be built and run by
# executing the command:
#
#    scons test_multibyte_add
#
# where "test_multibyte_add" is the alias for the SCons 'RunUnitTests' target.

# List of executable files
run_sources = [
    test_multibyte_add,
    test_multibyte_subtract,
    test_multibyte_multiply,
    test_multibyte_shift,
    test_fixedpoint32_multiply,
    test_fixedpoint32_add,
    test_fixedpoint32_subtract
]

# Auto-generate a 'RunUnitTests' target and Alias for each executable file
gcda_run_targets = []
xml_run_targets = []
for source in run_sources:
    source_path = str(source[0])
    alias = os.path.splitext(os.path.basename(source_path))[0]  # base name without directory path or file extension
    gcda_run_target = os.path.splitext(source_path)[0] + '.gcda'
    xml_run_target = os.path.join('test-reports', alias + '.xml')
    gcda_run_targets.append(gcda_run_target)
    xml_run_targets.append(xml_run_target)

    # RunUnitTests Builder generates two targets: .xml and .gcda
    vars()[alias] = env.RunUnitTests([xml_run_target, gcda_run_target], source)

    env.Alias(alias, vars()[alias])
    env.AlwaysBuild(vars()[alias])
    # All run targets share a single dummy gcda_mutex file, to prevent parallel
    # execution that can corrupt the test coverage statistics files (.gcda)
    env.SideEffect('gcda_mutex', vars()[alias])

# Create an alias to run all unit tests
env.Alias('run', xml_run_targets + gcda_run_targets)


##############################################################################
# COVERAGE TARGET DEFINITIONS
##############################################################################

# Generate HTML unit test coverage reports
html_coverage = env.Command(target = 'coverage-reports/coverage.html',
                            source = gcda_run_targets,
                            action = 'gcovr -r ./ -s --html --html-details -o $TARGET --html-title "Multibyte Unit Tests" build -f source -f test/test.c')
env.AlwaysBuild(html_coverage)
env.Alias('html_coverage', html_coverage)


# Generate XML unit test coverage report
xml_coverage = env.Command(target = 'coverage-reports/coverage.xml',
                           source = gcda_run_targets,
                           action = 'gcovr -r ./ -s --xml --xml-pretty -o $TARGET build -f source -f test/test.c')
env.AlwaysBuild(xml_coverage)
env.Alias('xml_coverage', xml_coverage)

# Create an alias to build all unit test coverage reports
env.Alias('coverage', html_coverage + xml_coverage)


##############################################################################
# CLEAN TARGET DEFINITIONS
##############################################################################

# Clean actions for unit test coverage artefacts
for file in glob.glob('coverage-reports/coverage*.html'):
    env.Clean(html_coverage, file)
for file in glob.glob('**/*.gcno'):
    env.Clean(html_coverage, file)
for file in glob.glob('**/*.gcda'):
    env.Clean(html_coverage, file)
